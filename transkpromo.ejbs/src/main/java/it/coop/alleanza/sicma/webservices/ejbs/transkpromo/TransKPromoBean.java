package it.coop.alleanza.sicma.webservices.ejbs.transkpromo;

import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import it.coop.alleanza.sicma.webservices.ejbs.transkpromo.dto.TransCodifica;
import it.coop.alleanza.sicma.webservices.ejbs.transkpromo.dto.TransKPromoINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.transkpromo.dto.TransKPromoOUTPUT;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author antonio.caccamo
 *
 * <pre>
 *  CREATE OR REPLACE TYPE DEC_ANAG_RECORD IS OBJECT (
 *      COD_ANAGR_SICMA     NUMBER,
 *      COD_ANAGR_NEGOZIO   NUMBER
 *  );
 *
 *  CREATE OR REPLACE TYPE DEC_ANAG_TABLE IS TABLE OF DEC_ANAG_RECORD
 *  ;
 *
 *  CREATE OR REPLACE TYPE DEC_ANAG_INPUT IS OBJECT (
 *           CODICE_AZIENDA      VARCHAR2(3),
 *           CODICE_CANALE       VARCHAR2(5),
 *           CODICE_NEGOZIO      VARCHAR2(5),
 *           TIPO_DATO           VARCHAR2(10),
 *           REQUEST_ID          NUMBER,
 *           CLIENT            VARCHAR2(10),
 *           VERSIONE            VARCHAR2(10),
 *           USERCODEID          NUMBER,
 *           DECOD_REQUEST_TAB   DEC_ANAG_TABLE
 *     );
 *
 *     CREATE OR REPLACE TYPE DEC_ANAG_OUTPUT IS OBJECT (
 *           CODICE_AZIENDA      VARCHAR2(3),
 *           CODICE_CANALE       VARCHAR2(5),
 *           CODICE_NEGOZIO      VARCHAR2(5),
 *           REQUEST_ID          NUMBER,
 *           DECOD_REQUEST_TAB   DEC_ANAG_TABLE,
 *           RETURN_STATUS       NUMBER,
 *           RETURN_CODE         VARCHAR2(3),
 *           MESSAGGIO           VARCHAR2(516)
 *     );
 *
 *  CREATE OR REPLACE PACKAGE WS_TRANSCODIFICAKPROMO IS
 *
 *    FUNCTION TRANSCODIFICAKPROMO (P_REQ DEC_ANAG_INPUT ) RETURN DEC_ANAG_OUTPUT;
 *
 *  END WS_TRANSCODIFICAKPROMO;
 *
 * </pre>
 */
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@Stateless(name = "TransKPromoBean", mappedName = "ejb/TransKPromoBean")
@Remote(TransKPromoBeanRemote.class)
public class TransKPromoBean implements TransKPromoBeanRemote {

    private static final Logger logger = LoggerFactory.getLogger(TransKPromoBean.class);

    //
    private static final String DEC_ANAG_OUTPUT = "DEC_ANAG_OUTPUT";
    private static final String DEC_ANAG_INPUT  = "DEC_ANAG_INPUT";
    private static final String DEC_ANAG_TABLE  = "DEC_ANAG_TABLE";
    private static final String DEC_ANAG_RECORD = "DEC_ANAG_RECORD";
    
    // DEC_ANAG_OUTPUT
    private static final String CODICE_AZIENDA    = "CODICE_AZIENDA";
    private static final String CODICE_CANALE     = "CODICE_CANALE";
    private static final String CODICE_NEGOZIO    = "CODICE_NEGOZIO";
    private static final String REQUEST_ID        = "REQUEST_ID";
    private static final String DECOD_REQUEST_TAB = "DECOD_REQUEST_TAB";
    private static final String RETURN_STATUS     = "RETURN_STATUS";
    private static final String RETURN_CODE       = "RETURN_CODE";
    private static final String MESSAGGIO         = "MESSAGGIO";
    
    // DEC_ANAG_INPUT
    //private static final String CODICE_AZIENDA    = "CODICE_AZIENDA";
    //private static final String CODICE_CANALE     = "CODICE_CANALE";
    //private static final String CODICE_NEGOZIO    = "CODICE_NEGOZIO";
    private static final String TIPO_DATO         = "TIPO_DATO";
    //private static final String REQUEST_ID        = "REQUEST_ID";
    private static final String CLIENT            = "CLIENT";
    private static final String VERSIONE          = "VERSIONE";
    private static final String USERCODEID        = "USERCODEID";
    //
    private static final String COD_ANAGR_SICMA   = "COD_ANAGR_SICMA";
    private static final String COD_ANAGR_NEGOZIO = "COD_ANAGR_NEGOZIO";

    @Resource(name = "jdbc/iper", mappedName = "jdbc/iper")
    DataSource iperDataSource;

    @Resource(name = "jdbc/super", mappedName = "jdbc/super")
    DataSource superDataSource;

    @Override
    public TransKPromoOUTPUT callTransKPromoStoreProcedureIPER(TransKPromoINPUT input) throws SicmaEJBException {
        logger.info("begin   callTransKPromoStoreProcedureIPER ..");
        try {
            final Connection connection = iperDataSource.getConnection();
            return callTransKPromoStoreProcedure(input, connection);
        } catch (Exception e) {
            logger.error("error occurred : " + e);
            throw new SicmaEJBException(e);
        } finally {
            logger.info("end   callTransKPromoStoreProcedureIPER ..");
        }
    }

    @Override
    public TransKPromoOUTPUT callTransKPromoStoreProcedureSUPER(TransKPromoINPUT input) throws SicmaEJBException {
        logger.info("begin   callTransKPromoStoreProcedureSUPER ..");
        try {
            final Connection connection = superDataSource.getConnection();
            return callTransKPromoStoreProcedure(input, connection);
        } catch (Exception e) {
            logger.error("error occurred : " + e);
            throw new SicmaEJBException(e);
        } finally {
            logger.info("end   callTransKPromoStoreProcedureSUPER ..");
        }
    }
    //private static final String DECOD_REQUEST_TAB = "DECOD_REQUEST_TAB";

    private TransKPromoOUTPUT callTransKPromoStoreProcedure(TransKPromoINPUT input, Connection connection) throws Exception {

        final StructDescriptor decAnagRecordDescriptor = StructDescriptor.createDescriptor(DEC_ANAG_RECORD, connection);
        final ArrayDescriptor decAnagTableDescriptor = ArrayDescriptor.createDescriptor(DEC_ANAG_TABLE, connection);
        final StructDescriptor decAnagInputDescriptor = StructDescriptor.createDescriptor(DEC_ANAG_INPUT, connection);
        final StructDescriptor decAnagOutputDescriptor = StructDescriptor.createDescriptor(DEC_ANAG_OUTPUT, connection);
        final Object[] inputObject = createStoreProcedureInputObject(input, connection, decAnagTableDescriptor, decAnagRecordDescriptor);
        final Struct inputStruct = new STRUCT(decAnagInputDescriptor, connection, inputObject);
        final CallableStatement call = connection.prepareCall("{ ? = call WS_TRANSCODIFICAKPROMO.TRANSCODIFICAKPROMO(?) }");
        call.registerOutParameter(1, Types.STRUCT, DEC_ANAG_OUTPUT);
        call.setObject(2, inputStruct);
        call.execute();
        final Struct outputStruct = (Struct) call.getObject(1);

        final TransKPromoOUTPUT output
                = retrieveTransKPromoOUTPUT(outputStruct, decAnagOutputDescriptor.getMetaData(), decAnagRecordDescriptor.getMetaData());
        return output;
    }

    private TransKPromoOUTPUT retrieveTransKPromoOUTPUT(Struct outputStruct, ResultSetMetaData decAnagOutputMetaData, ResultSetMetaData decAnagRecordMetaData) throws Exception {
        TransKPromoOUTPUT output = new TransKPromoOUTPUT();
        int i = 1;
        for (Object attribute : outputStruct.getAttributes()) {
            if (StringUtils.equalsIgnoreCase(decAnagOutputMetaData.getColumnName(i), CODICE_AZIENDA)) {
                output.setCodiceAzienda((String) attribute);
            }
            if (StringUtils.equalsIgnoreCase(decAnagOutputMetaData.getColumnName(i), CODICE_CANALE)) {
                output.setCodiceCanale((String) attribute);
            }
            if (StringUtils.equalsIgnoreCase(decAnagOutputMetaData.getColumnName(i), CODICE_NEGOZIO)) {
                output.setCodiceNegozio((String) attribute);
            }
            if (StringUtils.equalsIgnoreCase(decAnagOutputMetaData.getColumnName(i), REQUEST_ID)) {
                BigDecimal bigDecimal = (BigDecimal) attribute;
                output.setRequestId(Integer.valueOf(bigDecimal.intValue()));
            }
            if (StringUtils.equalsIgnoreCase(decAnagOutputMetaData.getColumnName(i), DECOD_REQUEST_TAB)) {
                output.setTransCodifiche(new ArrayList<TransCodifica>());
                Object[] data = (Object[]) ((Array) attribute).getArray();
                for (Object tmp : data) {
                    TransCodifica tc = new TransCodifica();
                    Struct rec = (Struct) tmp;
                    int j = 1;
                    for (Object recAttribute : rec.getAttributes()) {
                        if (StringUtils.equalsIgnoreCase(decAnagRecordMetaData.getColumnName(j), COD_ANAGR_SICMA)) {
                            BigDecimal bigDecimal = (BigDecimal) recAttribute;
                            tc.setCodAnagSicma(Integer.valueOf(bigDecimal.intValue()));
                        }
                        if (StringUtils.equalsIgnoreCase(decAnagRecordMetaData.getColumnName(j), COD_ANAGR_NEGOZIO)) {
                            BigDecimal bigDecimal = (BigDecimal) recAttribute;
                            tc.setCodAnagNegozio(Integer.valueOf(bigDecimal.intValue()));
                        }
                        j++;
                    }
                    output.getTransCodifiche().add(tc);
                }
            }
            if (StringUtils.equalsIgnoreCase(decAnagOutputMetaData.getColumnName(i), RETURN_STATUS)) {
                BigDecimal bigDecimal = (BigDecimal) attribute;
                output.setReturnStatus(Integer.valueOf(bigDecimal.intValue()));
            }
            if (StringUtils.equalsIgnoreCase(decAnagOutputMetaData.getColumnName(i), RETURN_CODE)) {
                output.setReturnCode((String) attribute);
            }
            if (StringUtils.equalsIgnoreCase(decAnagOutputMetaData.getColumnName(i), MESSAGGIO)) {
                output.setMessaggio((String) attribute);
            }
            i++;
        }
        return output;
    }

    private Object[] createStoreProcedureInputObject(TransKPromoINPUT input, Connection connection, ArrayDescriptor arrayDescriptor, StructDescriptor structDescriptor) throws SQLException {
        Object[] object = new Object[9]; // nove parametri
        object[0] = input.getCodiceAzienda();
        object[1] = input.getCodiceCanale();
        object[2] = input.getCodiceNegozio();
        object[3] = input.getTipoDato();
        object[4] = input.getRequestId();
        object[5] = input.getClient();
        object[6] = input.getVersione();
        object[7] = input.getUserCodeID();
        Object[] transCodeTable = new Object[input.getTransCodifiche().size()];
        Object[][] transCodeRecords = new Object[input.getTransCodifiche().size()][2];
        for (int i = 0; i < input.getTransCodifiche().size(); i++) {
            TransCodifica tc = input.getTransCodifiche().get(i);
            transCodeRecords[i][0] = tc.getCodAnagSicma();
            transCodeRecords[i][1] = tc.getCodAnagNegozio();
            transCodeTable[i] = new STRUCT(structDescriptor, connection, transCodeRecords[i]);
        }
        object[8] = new ARRAY(arrayDescriptor, connection, transCodeTable);
        return object;
    }

}
