/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.coop.alleanza.sicma.webservices.ejbs;

import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import it.coop.alleanza.sicma.webservices.ejbs.transkpromo.TransKPromoBeanRemote;

/**
 *
 * @author antonio.caccamo
 */
public  abstract class EjbAbstractFactory {        
       
    public abstract TransKPromoBeanRemote getTransKPromoBean()   throws SicmaEJBException ;


}
