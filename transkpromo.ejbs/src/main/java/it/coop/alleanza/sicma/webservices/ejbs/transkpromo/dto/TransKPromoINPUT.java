
package it.coop.alleanza.sicma.webservices.ejbs.transkpromo.dto;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class TransKPromoINPUT implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    private Integer requestId;
    /**
     * 
     * (Required)
     * 
     */
    private String client;
    /**
     * 
     * (Required)
     * 
     */
    private String versione;
    /**
     * 
     * (Required)
     * 
     */
    private Integer userCodeID;
    /**
     * 
     * (Required)
     * 
     */
    private String codiceAzienda;
    /**
     * 
     * (Required)
     * 
     */
    private String codiceCanale;
    /**
     * 
     * (Required)
     * 
     */
    private String codiceNegozio;
    /**
     * 
     * (Required)
     * 
     */
    private String tipoDato;
    private List<TransCodifica> transCodifiche = new ArrayList<TransCodifica>();
    private final static long serialVersionUID = -2515543983554097011L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public TransKPromoINPUT() {
    }

    /**
     * 
     * @param transCodifiche
     * @param codiceCanale
     * @param client
     * @param requestId
     * @param versione
     * @param tipoDato
     * @param codiceAzienda
     * @param userCodeID
     * @param codiceNegozio
     */
    public TransKPromoINPUT(Integer requestId, String client, String versione, Integer userCodeID, String codiceAzienda, String codiceCanale, String codiceNegozio, String tipoDato, List<TransCodifica> transCodifiche) {
        super();
        this.requestId = requestId;
        this.client = client;
        this.versione = versione;
        this.userCodeID = userCodeID;
        this.codiceAzienda = codiceAzienda;
        this.codiceCanale = codiceCanale;
        this.codiceNegozio = codiceNegozio;
        this.tipoDato = tipoDato;
        this.transCodifiche = transCodifiche;
    }

    /**
     * 
     * (Required)
     * 
     */

    public Integer getRequestId() {
        return requestId;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public TransKPromoINPUT withRequestId(Integer requestId) {
        this.requestId = requestId;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */

    public String getClient() {
        return client;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setClient(String client) {
        this.client = client;
    }

    public TransKPromoINPUT withClient(String client) {
        this.client = client;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */

    public String getVersione() {
        return versione;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setVersione(String versione) {
        this.versione = versione;
    }

    public TransKPromoINPUT withVersione(String versione) {
        this.versione = versione;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */

    public Integer getUserCodeID() {
        return userCodeID;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setUserCodeID(Integer userCodeID) {
        this.userCodeID = userCodeID;
    }

    public TransKPromoINPUT withUserCodeID(Integer userCodeID) {
        this.userCodeID = userCodeID;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */

    public String getCodiceAzienda() {
        return codiceAzienda;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setCodiceAzienda(String codiceAzienda) {
        this.codiceAzienda = codiceAzienda;
    }

    public TransKPromoINPUT withCodiceAzienda(String codiceAzienda) {
        this.codiceAzienda = codiceAzienda;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */

    public String getCodiceCanale() {
        return codiceCanale;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setCodiceCanale(String codiceCanale) {
        this.codiceCanale = codiceCanale;
    }

    public TransKPromoINPUT withCodiceCanale(String codiceCanale) {
        this.codiceCanale = codiceCanale;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @XmlElement(name="codiceNegozio")
    public String getCodiceNegozio() {
        return codiceNegozio;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setCodiceNegozio(String codiceNegozio) {
        this.codiceNegozio = codiceNegozio;
    }

    public TransKPromoINPUT withCodiceNegozio(String codiceNegozio) {
        this.codiceNegozio = codiceNegozio;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @XmlElement(name="tipoDato")
    public String getTipoDato() {
        return tipoDato;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    public TransKPromoINPUT withTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
        return this;
    }

    @XmlElement(name="transCodifiche")
    public List<TransCodifica> getTransCodifiche() {
        return transCodifiche;
    }

    public void setTransCodifiche(List<TransCodifica> transCodifiche) {
        this.transCodifiche = transCodifiche;
    }

    public TransKPromoINPUT withTransCodifiche(List<TransCodifica> transCodifiche) {
        this.transCodifiche = transCodifiche;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(requestId).append(client).append(versione).append(userCodeID).append(codiceAzienda).append(codiceCanale).append(codiceNegozio).append(tipoDato).append(transCodifiche).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TransKPromoINPUT) == false) {
            return false;
        }
        TransKPromoINPUT rhs = ((TransKPromoINPUT) other);
        return new EqualsBuilder().append(requestId, rhs.requestId).append(client, rhs.client).append(versione, rhs.versione).append(userCodeID, rhs.userCodeID).append(codiceAzienda, rhs.codiceAzienda).append(codiceCanale, rhs.codiceCanale).append(codiceNegozio, rhs.codiceNegozio).append(tipoDato, rhs.tipoDato).append(transCodifiche, rhs.transCodifiche).isEquals();
    }

}
