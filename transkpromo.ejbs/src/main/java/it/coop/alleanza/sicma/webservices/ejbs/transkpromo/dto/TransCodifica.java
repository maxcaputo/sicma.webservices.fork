
package it.coop.alleanza.sicma.webservices.ejbs.transkpromo.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

public class TransCodifica implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    private Integer codAnagSicma;
    /**
     * 
     * (Required)
     * 
     */
    private Integer codAnagNegozio;
    private final static long serialVersionUID = -4565963542674868712L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public TransCodifica() {
    }

    /**
     * 
     * @param codAnagNegozio
     * @param codAnagSicma
     */
    public TransCodifica(Integer codAnagSicma, Integer codAnagNegozio) {
        super();
        this.codAnagSicma = codAnagSicma;
        this.codAnagNegozio = codAnagNegozio;
    }

    /**
     * 
     * (Required)
     * 
     */

    public Integer getCodAnagSicma() {
        return codAnagSicma;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setCodAnagSicma(Integer codAnagSicma) {
        this.codAnagSicma = codAnagSicma;
    }

    public TransCodifica withCodAnagSicma(Integer codAnagSicma) {
        this.codAnagSicma = codAnagSicma;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */

    public Integer getCodAnagNegozio() {
        return codAnagNegozio;
    }

    /**
     * 
     * (Required)
     * 
     */
    
    public void setCodAnagNegozio(Integer codAnagNegozio) {
        this.codAnagNegozio = codAnagNegozio;
    }

    public TransCodifica withCodAnagNegozio(Integer codAnagNegozio) {
        this.codAnagNegozio = codAnagNegozio;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(codAnagSicma).append(codAnagNegozio).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TransCodifica) == false) {
            return false;
        }
        TransCodifica rhs = ((TransCodifica) other);
        return new EqualsBuilder().append(codAnagSicma, rhs.codAnagSicma).append(codAnagNegozio, rhs.codAnagNegozio).isEquals();
    }

}
