package it.coop.alleanza.sicma.webservices.ejbs.transkpromo.dto;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author antonio.caccamo
 */

@XmlRootElement
public class TransKPromoOUTPUT implements Serializable
{

    /**
     *
     * (Required)
     *
     */
    private Integer requestId;
    /**
     *
     * (Required)
     *
     */
    private Integer returnStatus;
    private String returnCode;
    private String messaggio;
    private String codiceAzienda;
    private String codiceCanale;
    private String codiceNegozio;
    private List<TransCodifica> transCodifiche = new ArrayList<TransCodifica>();
    private final static long serialVersionUID = -4440888847437165938L;

    /**
     * No args constructor for use in serialization
     *
     */
    public TransKPromoOUTPUT() {
    }

    /**
     *
     * @param returnCode
     * @param transCodifiche
     * @param codiceCanale
     * @param requestId
     * @param codiceAzienda
     * @param returnStatus
     * @param messaggio
     * @param codiceNegozio
     */
    public TransKPromoOUTPUT(Integer requestId, Integer returnStatus, String returnCode, String messaggio, String codiceAzienda, String codiceCanale, String codiceNegozio, List<TransCodifica> transCodifiche) {
        super();
        this.requestId = requestId;
        this.returnStatus = returnStatus;
        this.returnCode = returnCode;
        this.messaggio = messaggio;
        this.codiceAzienda = codiceAzienda;
        this.codiceCanale = codiceCanale;
        this.codiceNegozio = codiceNegozio;
        this.transCodifiche = transCodifiche;
    }

    /**
     *
     * (Required)
     *
     */
    @XmlElement(name="requestId")
    public Integer getRequestId() {
        return requestId;
    }

    /**
     *
     * (Required)
     *
     */
    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public TransKPromoOUTPUT withRequestId(Integer requestId) {
        this.requestId = requestId;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @XmlElement(name="returnStatus")
    public Integer getReturnStatus() {
        return returnStatus;
    }

    /**
     *
     * (Required)
     *
     */
    public void setReturnStatus(Integer returnStatus) {
        this.returnStatus = returnStatus;
    }

    public TransKPromoOUTPUT withReturnStatus(Integer returnStatus) {
        this.returnStatus = returnStatus;
        return this;
    }

    @XmlElement(name ="returnCode") public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public TransKPromoOUTPUT withReturnCode(String returnCode) {
        this.returnCode = returnCode;
        return this;
    }

    @XmlElement(name="messaggio")
    public String getMessaggio() {
        return messaggio;
    }

    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

    public TransKPromoOUTPUT withMessaggio(String messaggio) {
        this.messaggio = messaggio;
        return this;
    }

    @XmlElement(name="codiceAzienda")
    public String getCodiceAzienda() {
        return codiceAzienda;
    }

    public void setCodiceAzienda(String codiceAzienda) {
        this.codiceAzienda = codiceAzienda;
    }

    public TransKPromoOUTPUT withCodiceAzienda(String codiceAzienda) {
        this.codiceAzienda = codiceAzienda;
        return this;
    }

    @XmlElement(name="codiceCanale")
    public String getCodiceCanale() {
        return codiceCanale;
    }

    public void setCodiceCanale(String codiceCanale) {
        this.codiceCanale = codiceCanale;
    }

    public TransKPromoOUTPUT withCodiceCanale(String codiceCanale) {
        this.codiceCanale = codiceCanale;
        return this;
    }

    @XmlElement(name="codiceNegozio")
    public String getCodiceNegozio() {
        return codiceNegozio;
    }

    public void setCodiceNegozio(String codiceNegozio) {
        this.codiceNegozio = codiceNegozio;
    }

    public TransKPromoOUTPUT withCodiceNegozio(String codiceNegozio) {
        this.codiceNegozio = codiceNegozio;
        return this;
    }

    @XmlElement(name="transCodifiche")
    public List<TransCodifica>  getTransCodifiche() {
        return transCodifiche;
    }

    public void setTransCodifiche(List<TransCodifica>  transCodifiche) {
        this.transCodifiche = transCodifiche;
    }

    public TransKPromoOUTPUT withTransCodifiche(List<TransCodifica>  transCodifiche) {
        this.transCodifiche = transCodifiche;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(requestId).append(returnStatus).append(returnCode).append(messaggio).append(codiceAzienda).append(codiceCanale).append(codiceNegozio).append(transCodifiche).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TransKPromoOUTPUT) == false) {
            return false;
        }
        TransKPromoOUTPUT rhs = ((TransKPromoOUTPUT) other);
        return new EqualsBuilder().append(requestId, rhs.requestId).append(returnStatus, rhs.returnStatus).append(returnCode, rhs.returnCode).append(messaggio, rhs.messaggio).append(codiceAzienda, rhs.codiceAzienda).append(codiceCanale, rhs.codiceCanale).append(codiceNegozio, rhs.codiceNegozio).append(transCodifiche, rhs.transCodifiche).isEquals();
    }

}

