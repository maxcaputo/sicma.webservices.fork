/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.coop.alleanza.sicma.webservices.ejbs.transkpromo;

import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import it.coop.alleanza.sicma.webservices.ejbs.transkpromo.dto.TransKPromoINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.transkpromo.dto.TransKPromoOUTPUT;
import it.coop.alleanza.sicma.webservices.model.DecodificaAnagrafica;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author antonio.caccamo
 */
@Remote
public interface TransKPromoBeanRemote {

    public TransKPromoOUTPUT callTransKPromoStoreProcedureIPER(TransKPromoINPUT input) throws SicmaEJBException;

    public TransKPromoOUTPUT callTransKPromoStoreProcedureSUPER(TransKPromoINPUT input) throws SicmaEJBException;
}
