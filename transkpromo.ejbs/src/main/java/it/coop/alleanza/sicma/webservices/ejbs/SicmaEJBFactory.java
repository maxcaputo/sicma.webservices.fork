/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.coop.alleanza.sicma.webservices.ejbs;

import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import it.coop.alleanza.sicma.webservices.ejbs.transkpromo.TransKPromoBeanRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author antonio.caccamo
 */
public class SicmaEJBFactory extends EjbAbstractFactory {
    
    private static final Logger logger = LoggerFactory.getLogger(SicmaEJBFactory.class);

  private static final String TRANSKPROMO =
            "ejb.TransKPromoBean#it.coop.alleanza.sicma.webservices.ejbs.transkpromo.TransKPromoBeanRemote";

   private static SicmaEJBFactory instance = null;

    private final InitialContext context;

    private SicmaEJBFactory() throws SicmaEJBException {
        try {
            context = new InitialContext();
        } catch (NamingException ex) {
            logger.error("error occurred during initializing context for ejb : ", ex);
            throw new SicmaEJBException(ex);
        }
    }

    public static synchronized SicmaEJBFactory getInstance() throws SicmaEJBException {
        if (instance == null) {
            instance = new SicmaEJBFactory();
        }
        return instance;
    }

    @Override
    public TransKPromoBeanRemote getTransKPromoBean() throws SicmaEJBException {
        return (TransKPromoBeanRemote) lookup(SicmaEJBFactory.TRANSKPROMO);
    }



    private Object lookup(String name) throws SicmaEJBException {
        try {
            return context.lookup(name);
        } catch (NamingException ex) {
            logger.error("error occurred during context for ejb " +  name + " : ", ex);
            throw new SicmaEJBException(ex);
        }
    }

}
