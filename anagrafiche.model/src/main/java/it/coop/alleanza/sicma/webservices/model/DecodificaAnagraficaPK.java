/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.coop.alleanza.sicma.webservices.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author antonio.caccamo
 */

public class DecodificaAnagraficaPK implements Serializable {

     
    private String tipoDato;

     
    private Integer codiceCooperativa;

     
    private Integer codiceNegozio;

     
    private Integer CodiceAngraficaNegozio;

    public String getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    public Integer getCodiceCooperativa() {
        return codiceCooperativa;
    }

    public void setCodiceCooperativa(Integer codiceCooperativa) {
        this.codiceCooperativa = codiceCooperativa;
    }

    public Integer getCodiceNegozio() {
        return codiceNegozio;
    }

    public void setCodiceNegozio(Integer codiceNegozio) {
        this.codiceNegozio = codiceNegozio;
    }

    public Integer getCodiceAngraficaNegozio() {
        return CodiceAngraficaNegozio;
    }

    public void setCodiceAngraficaNegozio(Integer CodiceAngraficaNegozio) {
        this.CodiceAngraficaNegozio = CodiceAngraficaNegozio;
    } 

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DecodificaAnagraficaPK other = (DecodificaAnagraficaPK) obj;
        if ((this.tipoDato == null) ? (other.tipoDato != null) : !this.tipoDato.equals(other.tipoDato)) {
            return false;
        }
        if (this.codiceCooperativa != other.codiceCooperativa && (this.codiceCooperativa == null || !this.codiceCooperativa.equals(other.codiceCooperativa))) {
            return false;
        }
        if (this.codiceNegozio != other.codiceNegozio && (this.codiceNegozio == null || !this.codiceNegozio.equals(other.codiceNegozio))) {
            return false;
        }
        if (this.CodiceAngraficaNegozio != other.CodiceAngraficaNegozio && (this.CodiceAngraficaNegozio == null || !this.CodiceAngraficaNegozio.equals(other.CodiceAngraficaNegozio))) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public String toString() {
        return "DecodificaAnagraficaPK{" + "tipoDato=" + tipoDato + ", codiceCooperativa=" + codiceCooperativa + ", codiceNegozio=" + codiceNegozio + ", CodiceAngraficaNegozio=" + CodiceAngraficaNegozio + '}';
    }
        
    
}
