package it.coop.alleanza.sicma.webservices.ejbs.checkupdates;

import it.coop.alleanza.sicma.webservices.ejbs.AbstractAnagrafiche;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.dto.CheckUpdatesOUTPUT;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.sql.*;

/**
 * Created by antonio.caccamo on 30/01/2017.
 */
@Stateless(name = "CheckUpdatesBean", mappedName = "ejb/CheckUpdatesBean")
@Remote(CheckUpdatesBeanRemote.class)
public class CheckUpdatesBean extends AbstractAnagrafiche implements CheckUpdatesBeanRemote{


    private static final Logger logger = LoggerFactory.getLogger(CheckUpdatesBean.class);

    @PersistenceContext(unitName = "sicma-unit")
    EntityManager em;

    @Resource(mappedName="jdbc/sicma")
    DataSource dataSource;

    @Override
    public CheckUpdatesOUTPUT callCheckUpdatesStoreFunction(AnagraficheINPUT input) throws SicmaEJBException {
        logger.info("begin callCheckUpdatesStoreFunction ..");
        CheckUpdatesOUTPUT output;
        try {
//          final Connection connection = em.unwrap(Connection.class);
            final Connection connection = dataSource.getConnection();
            final StructDescriptor structInputDescriptor    = StructDescriptor.createDescriptor(AbstractAnagrafiche.ANAG_RECORD_INPUT , connection);
            final StructDescriptor structOutputDescriptor   = StructDescriptor.createDescriptor(AbstractAnagrafiche.CHECK_UPD_OUTPUT  , connection);
            final StructDescriptor structDettaglioDescriptor = StructDescriptor.createDescriptor(AbstractAnagrafiche.CHECK_UPD_DET_REC  , connection);
            final Object[] objectIn = getStoreProcedureInputObject(input);

            final Struct structIn = new STRUCT(structInputDescriptor, connection, objectIn);
            CallableStatement call = connection.prepareCall("{ ? = call WS_CHECKUPDATES.CHECKUPDATES(?) }");
            call.registerOutParameter(1, Types.STRUCT, AbstractAnagrafiche.CHECK_UPD_OUTPUT);
            call.setObject(2, structIn);
            call.execute();
            final Struct row = (Struct) call.getObject(1);
            final ResultSetMetaData outputMetaData = structOutputDescriptor.getMetaData();
            final ResultSetMetaData dettaglioMetaData = structDettaglioDescriptor.getMetaData();
            output = getCheckUpdatesOUTPUT(row, outputMetaData, dettaglioMetaData);

        } catch (Exception e) {
            logger.error("error occurred : " + e);
            throw new SicmaEJBException(e);
        } finally {
            logger.info("end   callCheckUpdatesStoreFunction ..");
        }
        return output;
    }
}
