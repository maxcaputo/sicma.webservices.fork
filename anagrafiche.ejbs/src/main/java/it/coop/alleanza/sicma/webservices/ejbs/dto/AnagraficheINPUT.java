/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.coop.alleanza.sicma.webservices.ejbs.dto;

/**
 *
 * @author antonio.caccamo
 */

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
public class AnagraficheINPUT implements Serializable {

    public AnagraficheINPUT() {
    }
            

    /**
     *
     * (Required)
     *
     */
    private Integer requestId;
    /**
     *
     * (Required)
     *
     */
    private String client;
    /**
     *
     * (Required)
     *
     */
    private String versione;
    private Integer userCodeID;
    private final static long serialVersionUID = 5671088395982567733L;

    /**
     *
     * @param client
     * @param requestId
     * @param versione
     * @param userCodeID
     */
    public AnagraficheINPUT(Integer requestId, String client, String versione, Integer userCodeID) {
        super();
        this.requestId = requestId;
        this.client = client;
        this.versione = versione;
        this.userCodeID = userCodeID;
    }

    /**
     *
     * (Required)
     *
     */
    public Integer getRequestId() {
        return requestId;
    }

    /**
     *
     * (Required)
     *
     */
    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public AnagraficheINPUT withRequestId(Integer requestId) {
        this.requestId = requestId;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public String getClient() {
        return client;
    }

    /**
     *
     * (Required)
     *
     */
    public void setClient(String client) {
        this.client = client;
    }

    public AnagraficheINPUT withClient(String client) {
        this.client = client;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public String getVersione() {
        return versione;
    }

    /**
     *
     * (Required)
     *
     */
    public void setVersione(String versione) {
        this.versione = versione;
    }

    public AnagraficheINPUT withVersione(String versione) {
        this.versione = versione;
        return this;
    }

    public Integer getUserCodeID() {
        return userCodeID;
    }

    public void setUserCodeID(Integer userCodeID) {
        this.userCodeID = userCodeID;
    }

    public AnagraficheINPUT withUserCodeID(Integer userCodeID) {
        this.userCodeID = userCodeID;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(requestId).append(client).append(versione).append(userCodeID).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AnagraficheINPUT) == false) {
            return false;
        }
        AnagraficheINPUT rhs = ((AnagraficheINPUT) other);
        return new EqualsBuilder().append(requestId, rhs.requestId).append(client, rhs.client).append(versione, rhs.versione).append(userCodeID, rhs.userCodeID).isEquals();
    }

}
