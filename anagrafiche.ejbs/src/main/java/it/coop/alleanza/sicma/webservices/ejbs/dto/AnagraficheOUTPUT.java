
package it.coop.alleanza.sicma.webservices.ejbs.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * 
 * @author antonio.caccamo
 */
@XmlRootElement
public class AnagraficheOUTPUT implements Serializable
{
    
    

    private DettaglioFile dettaglioFile;
    /**
     * 
     * (Required)
     * 
     */
    private Integer requestId;
    /**
     * 
     * (Required)
     * 
     */
    private Integer returnStatus;
    private String returnCode;
    private String messaggio;
    private final static long serialVersionUID = -9119511859370473669L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AnagraficheOUTPUT() {
    }

    /**
     * 
     * @param returnCode
     * @param requestId
     * @param dettaglioFile
     * @param returnStatus
     * @param messaggio
     */
    public AnagraficheOUTPUT(DettaglioFile dettaglioFile, Integer requestId, Integer returnStatus, String returnCode, String messaggio) {
        super();
        this.dettaglioFile = dettaglioFile;
        this.requestId = requestId;
        this.returnStatus = returnStatus;
        this.returnCode = returnCode;
        this.messaggio = messaggio;
    }

    public DettaglioFile getDettaglioFile() {
        return dettaglioFile;
    }

    public void setDettaglioFile(DettaglioFile dettaglioFile) {
        this.dettaglioFile = dettaglioFile;
    }

    public AnagraficheOUTPUT withDettaglioFile(DettaglioFile dettaglioFile) {
        this.dettaglioFile = dettaglioFile;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Integer getRequestId() {
        return requestId;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public AnagraficheOUTPUT withRequestId(Integer requestId) {
        this.requestId = requestId;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Integer getReturnStatus() {
        return returnStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setReturnStatus(Integer returnStatus) {
        this.returnStatus = returnStatus;
    }

    public AnagraficheOUTPUT withReturnStatus(Integer returnStatus) {
        this.returnStatus = returnStatus;
        return this;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public AnagraficheOUTPUT withReturnCode(String returnCode) {
        this.returnCode = returnCode;
        return this;
    }

    public String getMessaggio() {
        return messaggio;
    }

    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

    public AnagraficheOUTPUT withMessaggio(String messaggio) {
        this.messaggio = messaggio;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dettaglioFile).append(requestId).append(returnStatus).append(returnCode).append(messaggio).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AnagraficheOUTPUT) == false) {
            return false;
        }
        AnagraficheOUTPUT rhs = ((AnagraficheOUTPUT) other);
        return new EqualsBuilder().append(dettaglioFile, rhs.dettaglioFile).append(requestId, rhs.requestId).append(returnStatus, rhs.returnStatus).append(returnCode, rhs.returnCode).append(messaggio, rhs.messaggio).isEquals();
    }

}
