
package it.coop.alleanza.sicma.webservices.ejbs.dto;

import javax.annotation.Generated;
import java.io.Serializable;

@Generated("org.jsonschema2pojo")
public class DettagliCheckUpdate implements Serializable {

    /**
     * 
     * (Required)
     * 
     */
    private String ultimoAggiornamento;
    /**
     * 
     * (Required)
     * 
     */
    private String nomeAnagrafica;

    /**
     * 
     * (Required)
     * 
     * @return
     *     The ultimoAggiornamento
     */
    public String getUltimoAggiornamento() {
        return ultimoAggiornamento;
    }

    /**
     * 
     * (Required)
     * 
     * @param ultimoAggiornamento
     *     The ultimoAggiornamento
     */
    public void setUltimoAggiornamento(String ultimoAggiornamento) {
        this.ultimoAggiornamento = ultimoAggiornamento;
    }

    public DettagliCheckUpdate withUltimoAggiornamento(String ultimoAggiornamento) {
        this.ultimoAggiornamento = ultimoAggiornamento;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The nomeAnagrafica
     */
    public String getNomeAnagrafica() {
        return nomeAnagrafica;
    }

    /**
     * 
     * (Required)
     * 
     * @param nomeAnagrafica
     *     The nomeAnagrafica
     */
    public void setNomeAnagrafica(String nomeAnagrafica) {
        this.nomeAnagrafica = nomeAnagrafica;
    }

    public DettagliCheckUpdate withNomeAnagrafica(String nomeAnagrafica) {
        this.nomeAnagrafica = nomeAnagrafica;
        return this;
    }

    @Override
    public String toString() {
        return "DettagliCheckUpdate{" +
                "ultimoAggiornamento='" + ultimoAggiornamento + '\'' +
                ", nomeAnagrafica='" + nomeAnagrafica + '\'' +
                '}';
    }
}
