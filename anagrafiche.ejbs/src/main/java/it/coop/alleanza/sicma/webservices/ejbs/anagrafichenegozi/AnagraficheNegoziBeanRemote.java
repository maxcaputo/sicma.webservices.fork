package it.coop.alleanza.sicma.webservices.ejbs.anagrafichenegozi;

import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheOUTPUT;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;

import javax.ejb.Remote;

/**
 * Created by antonio.caccamo on 30/01/2017.
 */
@Remote
public interface AnagraficheNegoziBeanRemote {

    public AnagraficheOUTPUT callAnagraficheNegoziStoreFunction(AnagraficheINPUT input) throws SicmaEJBException;
}
