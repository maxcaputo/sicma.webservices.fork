package it.coop.alleanza.sicma.webservices.ejbs;

import it.coop.alleanza.sicma.webservices.ejbs.dto.*;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.ResultSetMetaData;
import java.sql.Struct;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by antonio.caccamo on 30/01/2017.
 */
public abstract class AbstractAnagrafiche {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("YYYYMMDD");

    public static final String ANAG_RECORD_INPUT    = "ANAG_RECORD_INPUT";
    public static final String ANAG_RECORD_OUTPUT   = "ANAG_RECORD_OUTPUT";
    public static final  String CHECK_UPD_OUTPUT    = "CHECK_UPD_OUTPUT";
    public static final  String DETTAGLI            = "DETTAGLI";
    public static final  String CHECK_UPD_DET_REC = "CHECK_UPD_DET_REC";

    private static final String REQUESTID           = "REQUESTID";
    private static final String RETURNSTATUS        = "RETURNSTATUS";
    private static final String RETURNCODE          = "RETURNCODE";
    private static final String MESSAGGIO           = "MESSAGGIO";
    private static final String PATHFILE            = "PATHFILE";
    private static final String NOMEFILE            = "NOMEFILE";
    private static final String ULTIMOAGGIORNAMENTO = "ULTIMOAGGIORNAMENTO";
    private static final String RECORDCOUNT         = "RECORDCOUNT";
    private static final String NOMEANAGRAFICA      = "NOMEANAGRAFICA";

    public Object[] getStoreProcedureInputObject(AnagraficheINPUT input){
        final Object[] objectIn = new Object[4];
        objectIn[0] = input.getRequestId();
        objectIn[1] = input.getClient();
        objectIn[2] = input.getVersione();
        objectIn[3] = input.getUserCodeID();
        return objectIn;
    }

    public AnagraficheOUTPUT getAnagraficheOUTPUT(Struct row, ResultSetMetaData metaData) throws Exception {
        int idx = 1;
        AnagraficheOUTPUT output = new AnagraficheOUTPUT();
        for (Object attribute : row.getAttributes()) {
            //System.out.println(metaData.getColumnName(idx) + " = " + attribute);
            if (StringUtils.equalsIgnoreCase(metaData.getColumnName(idx), REQUESTID)) {
                BigDecimal bigDecimal = (BigDecimal) attribute;
                output.setRequestId(Integer.valueOf(bigDecimal.intValue()));
            }
            if (StringUtils.equalsIgnoreCase(metaData.getColumnName(idx), RETURNSTATUS)) {
                BigDecimal bigDecimal = (BigDecimal) attribute;
                output.setReturnStatus(Integer.valueOf(bigDecimal.intValue()));
            }
            if (StringUtils.equalsIgnoreCase(metaData.getColumnName(idx), RETURNCODE)) {
                String string = (String) attribute;
                output.setReturnCode(string);
            }
            if (StringUtils.equalsIgnoreCase(metaData.getColumnName(idx), MESSAGGIO)) {
                String string = (String) attribute;
                output.setMessaggio(string);
            }
            if (StringUtils.equalsIgnoreCase(metaData.getColumnName(idx), PATHFILE)) {
                String string = (String) attribute;
                if( output.getDettaglioFile() == null ){
                    output.setDettaglioFile( new DettaglioFile());
                }
                output.getDettaglioFile().setPathFile (string);
            }
            if (StringUtils.equalsIgnoreCase(metaData.getColumnName(idx), NOMEFILE)) {
                String string = (String) attribute;
                if( output.getDettaglioFile() == null ){
                    output.setDettaglioFile( new DettaglioFile());
                }
                output.getDettaglioFile().setNomeFile (string);
            }
            if (StringUtils.equalsIgnoreCase(metaData.getColumnName(idx), ULTIMOAGGIORNAMENTO)) {
                Date date = (Date) attribute;
                if( output.getDettaglioFile() == null ){
                    output.setDettaglioFile( new DettaglioFile());
                }
                output.getDettaglioFile().setUltimoAggiornamento(DATE_FORMAT.format(date));
            }
            if (StringUtils.equalsIgnoreCase(metaData.getColumnName(idx), RECORDCOUNT)) {
                BigDecimal bigDecimal = (BigDecimal) attribute;
                output.getDettaglioFile().setRecordCount(Integer.valueOf(bigDecimal.intValue()));
            }
            idx++;
        }
        return  output;
    }

    public CheckUpdatesOUTPUT getCheckUpdatesOUTPUT(Struct row, ResultSetMetaData outputMetaData, ResultSetMetaData dettaglioMetaData ) throws Exception{
        CheckUpdatesOUTPUT output = new CheckUpdatesOUTPUT();
        int i = 1;
        for (Object attribute : row.getAttributes()) {
            //System.out.println(metaData.getColumnName(i) + " = " + attribute);
            if (StringUtils.equalsIgnoreCase(outputMetaData.getColumnName(i), REQUESTID)) {
                BigDecimal bigDecimal = (BigDecimal) attribute;
                output.setRequestId(Integer.valueOf(bigDecimal.intValue()));
            }
            if (StringUtils.equalsIgnoreCase(outputMetaData.getColumnName(i), RETURNSTATUS)) {
                BigDecimal bigDecimal = (BigDecimal) attribute;
                output.setReturnStatus(Integer.valueOf(bigDecimal.intValue()));
            }
            if (StringUtils.equalsIgnoreCase(outputMetaData.getColumnName(i), RETURNCODE)) {
                String string = (String) attribute;
                output.setReturnCode(string);
            }
            if (StringUtils.equalsIgnoreCase(outputMetaData.getColumnName(i), MESSAGGIO)) {
                String string = (String) attribute;
                output.setMessaggio(string);
            }
            if (StringUtils.equalsIgnoreCase(outputMetaData.getColumnName(i), DETTAGLI)) {
                output.setDettagliCheckUpdates(new ArrayList<DettagliCheckUpdate>());
                Object[] data = (Object[]) ((Array) attribute).getArray();
                for (Object tmp : data) {
                    Struct rec = (Struct) tmp;
                    // Attributes are index 1 based...
                    DettagliCheckUpdate dettagliCheckUpdate = new DettagliCheckUpdate();
                    int j = 1;
                    for (Object recAttribute : rec.getAttributes()) {
                        if (StringUtils.equalsIgnoreCase(dettaglioMetaData.getColumnName(j), NOMEANAGRAFICA)) {
                            String string = (String) recAttribute;
                            dettagliCheckUpdate.setNomeAnagrafica(string);
                        }
                        if (StringUtils.equalsIgnoreCase(dettaglioMetaData.getColumnName(j), ULTIMOAGGIORNAMENTO)) {
                            Date date = (Date) recAttribute;
                            dettagliCheckUpdate.setUltimoAggiornamento(DATE_FORMAT.format(date));
                        }
                        j++;
                    }
                    output.getDettagliCheckUpdates().add(dettagliCheckUpdate);
                }
            }
            i++;
        }
        return output;
    }

}
