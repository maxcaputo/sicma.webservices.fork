
package it.coop.alleanza.sicma.webservices.ejbs.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * 
 * @author antonio.caccamo
 */
public class DettaglioFile implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    private String ultimoAggiornamento;
    /**
     * 
     * (Required)
     * 
     */
    private String nomeFile;
    /**
     * 
     * (Required)
     * 
     */
    private String pathFile;
    
    private Integer recordCount;

    public Integer getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(Integer recordCount) {
        this.recordCount = recordCount;
    }
    
    private final static long serialVersionUID = -7991517638896153496L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DettaglioFile() {
    }

    /**
     * 
     * @param nomeFile
     * @param pathFile
     * @param ultimoAggiornamento
     */
    public DettaglioFile(String ultimoAggiornamento, String nomeFile, String pathFile) {
        super();
        this.ultimoAggiornamento = ultimoAggiornamento;
        this.nomeFile = nomeFile;
        this.pathFile = pathFile;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getUltimoAggiornamento() {
        return ultimoAggiornamento;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setUltimoAggiornamento(String ultimoAggiornamento) {
        this.ultimoAggiornamento = ultimoAggiornamento;
    }

    public DettaglioFile withUltimoAggiornamento(String ultimoAggiornamento) {
        this.ultimoAggiornamento = ultimoAggiornamento;
        return this;
    }
    
    public DettaglioFile withRecordCount(Integer recordCount) {
        this.recordCount = recordCount;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getNomeFile() {
        return nomeFile;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setNomeFile(String nomeFile) {
        this.nomeFile = nomeFile;
    }

    public DettaglioFile withNomeFile(String nomeFile) {
        this.nomeFile = nomeFile;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getPathFile() {
        return pathFile;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public DettaglioFile withPathFile(String pathFile) {
        this.pathFile = pathFile;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(ultimoAggiornamento).append(nomeFile).append(pathFile).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DettaglioFile) == false) {
            return false;
        }
        DettaglioFile rhs = ((DettaglioFile) other);
        return new EqualsBuilder().append(ultimoAggiornamento, rhs.ultimoAggiornamento).append(nomeFile, rhs.nomeFile).append(pathFile, rhs.pathFile).isEquals();
    }

}
