/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.coop.alleanza.sicma.webservices.ejbs;

import it.coop.alleanza.sicma.webservices.ejbs.anagrafichenegozi.AnagraficheNegoziBeanRemote;
import it.coop.alleanza.sicma.webservices.ejbs.anagrafichesigle.AnagraficheSigleBeanRemote;
import it.coop.alleanza.sicma.webservices.ejbs.checkupdates.CheckUpdatesBeanRemote;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import it.coop.alleanza.sicma.webservices.ejbs.livellomerceologico.LivelloMerceologicoBeanRemote;

/**
 *
 * @author antonio.caccamo
 */
public  abstract class EjbAbstractFactory {        

    public abstract AnagraficheNegoziBeanRemote getAnagraficheNegoziBean() throws SicmaEJBException;

    public abstract AnagraficheSigleBeanRemote getAnagraficheSigleBean() throws SicmaEJBException;

    public abstract LivelloMerceologicoBeanRemote getLivelloMerceologicoBean() throws SicmaEJBException;

    public abstract CheckUpdatesBeanRemote getCheckUpdatesBeanRemote() throws SicmaEJBException;
}
