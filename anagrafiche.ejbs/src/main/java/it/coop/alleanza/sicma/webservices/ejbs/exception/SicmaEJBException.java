/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.coop.alleanza.sicma.webservices.ejbs.exception;

/**
 *
 * @author antonio.caccamo
 */
public class SicmaEJBException extends Exception {

    public SicmaEJBException(Exception e) {
        super(e.getMessage());
    }
    
}
