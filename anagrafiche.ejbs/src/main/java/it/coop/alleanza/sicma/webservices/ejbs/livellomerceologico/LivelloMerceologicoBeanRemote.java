package it.coop.alleanza.sicma.webservices.ejbs.livellomerceologico;

import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheOUTPUT;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;

import javax.ejb.Remote;

/**
 * Created by antonio.caccamo on 30/01/2017.
 */
@Remote
public interface LivelloMerceologicoBeanRemote {

    public AnagraficheOUTPUT callLivelloMerceologicoStoreFunction(AnagraficheINPUT input) throws SicmaEJBException;
}
