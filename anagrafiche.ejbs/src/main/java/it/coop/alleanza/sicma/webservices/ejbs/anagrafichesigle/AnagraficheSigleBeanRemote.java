package it.coop.alleanza.sicma.webservices.ejbs.anagrafichesigle;

import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheOUTPUT;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;

import javax.ejb.Remote;

/**
 * Created by antonio.caccamo on 30/01/2017.
 */
@Remote
public interface AnagraficheSigleBeanRemote {

    public AnagraficheOUTPUT callAnagraficheSigleStoreFunction(AnagraficheINPUT input) throws SicmaEJBException;
}
