package it.coop.alleanza.sicma.webservices.ejbs.checkupdates;

import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.dto.CheckUpdatesOUTPUT;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;

import javax.ejb.Remote;

/**
 * Created by antonio.caccamo on 30/01/2017.
 */
@Remote
public interface CheckUpdatesBeanRemote {

        public CheckUpdatesOUTPUT callCheckUpdatesStoreFunction(AnagraficheINPUT input) throws SicmaEJBException;
}
