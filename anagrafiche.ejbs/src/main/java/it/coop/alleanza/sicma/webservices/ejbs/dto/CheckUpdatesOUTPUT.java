
package it.coop.alleanza.sicma.webservices.ejbs.dto;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
@XmlRootElement
public class CheckUpdatesOUTPUT implements Serializable {

    private List<DettagliCheckUpdate> dettagliCheckUpdates = new ArrayList<DettagliCheckUpdate>();
    private String messaggio;
    /**
     * 
     * (Required)
     * 
     */
    private Integer requestId;
    /**
     * 
     * (Required)
     * 
     */
    private String returnCode;

    private Integer returnStatus;


    /**
     * 
     * @return
     *     The dettagliCheckUpdates
     */
    public List<DettagliCheckUpdate> getDettagliCheckUpdates() {
        return dettagliCheckUpdates;
    }

    /**
     * 
     * @param dettagliCheckUpdates
     *     The dettagliCheckUpdates
     */
    public void setDettagliCheckUpdates(List<DettagliCheckUpdate> dettagliCheckUpdates) {
        this.dettagliCheckUpdates = dettagliCheckUpdates;
    }

    public CheckUpdatesOUTPUT withDettagliCheckUpdates(List<DettagliCheckUpdate> dettagliCheckUpdates) {
        this.dettagliCheckUpdates = dettagliCheckUpdates;
        return this;
    }

    /**
     * 
     * @return
     *     The messaggio
     */
    public String getMessaggio() {
        return messaggio;
    }

    /**
     * 
     * @param messaggio
     *     The messaggio
     */
    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

    public CheckUpdatesOUTPUT withMessaggio(String messaggio) {
        this.messaggio = messaggio;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The requestId
     */
    public Integer getRequestId() {
        return requestId;
    }

    /**
     * 
     * (Required)
     * 
     * @param requestId
     *     The requestId
     */
    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public CheckUpdatesOUTPUT withRequestId(Integer requestId) {
        this.requestId = requestId;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The returnCode
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * 
     * (Required)
     * 
     * @param returnCode
     *     The returnCode
     */
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public CheckUpdatesOUTPUT withReturnCode(String returnCode) {
        this.returnCode = returnCode;
        return this;
    }

    public Integer getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(Integer returnStatus) {
        this.returnStatus = returnStatus;
    }

    public CheckUpdatesOUTPUT withReturnStatus(Integer returnStatus){
        this.returnStatus = returnStatus;
        return this;
    }

    @Override
    public String toString() {
        return "CheckUpdatesOUTPUT{" +
                "dettagliCheckUpdates=" + dettagliCheckUpdates +
                ", messaggio='" + messaggio + '\'' +
                ", requestId=" + requestId +
                ", returnCode='" + returnCode + '\'' +
                ", returnStatus=" + returnStatus +
                '}';
    }
}
