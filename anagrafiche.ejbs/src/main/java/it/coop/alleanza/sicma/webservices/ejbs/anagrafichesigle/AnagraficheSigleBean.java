package it.coop.alleanza.sicma.webservices.ejbs.anagrafichesigle;

import it.coop.alleanza.sicma.webservices.ejbs.AbstractAnagrafiche;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheOUTPUT;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.sql.*;

/**
 * Created by antonio.caccamo on 30/01/2017.
 */
@TransactionManagement
@Stateless(name = "AnagraficheSigleBean", mappedName = "ejb/AnagraficheSigleBean")
@Remote(AnagraficheSigleBeanRemote.class)
public class AnagraficheSigleBean  extends AbstractAnagrafiche implements AnagraficheSigleBeanRemote{

    private static final Logger logger = LoggerFactory.getLogger(AnagraficheSigleBeanRemote.class);

    @PersistenceContext(unitName = "sicma-unit")
    EntityManager em;

    @Resource(mappedName="jdbc/sicma")
    DataSource dataSource;


    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public AnagraficheOUTPUT callAnagraficheSigleStoreFunction(AnagraficheINPUT input) throws SicmaEJBException {
        AnagraficheOUTPUT output = null;
        logger.info("begin callAnagraficheSigleStoreFunction ..");
        try {
//          final Connection connection = em.unwrap(Connection.class);
            final Connection connection = dataSource.getConnection();
            final StructDescriptor structInputDescriptor  = StructDescriptor.createDescriptor(AbstractAnagrafiche.ANAG_RECORD_INPUT , connection);
            final StructDescriptor structOutputDescriptor = StructDescriptor.createDescriptor(AbstractAnagrafiche.ANAG_RECORD_OUTPUT, connection);
            final Object[] objectIn = getStoreProcedureInputObject(input);

            final Struct structIn = new STRUCT(structInputDescriptor, connection, objectIn);
            CallableStatement call = connection.prepareCall("{ ? = call WS_ANAGRAFICASIGLE.ANAGRAFICASIGLE(?) }");
            call.registerOutParameter(1, Types.STRUCT, AbstractAnagrafiche.ANAG_RECORD_OUTPUT);
            call.setObject(2, structIn);
            call.execute();
            final Struct row = (Struct) call.getObject(1);
            final ResultSetMetaData metaData = structOutputDescriptor.getMetaData();
            output = getAnagraficheOUTPUT(row, metaData);

        } catch (Exception e) {
            logger.error("error occurred : " + e);
            throw new SicmaEJBException(e);
        } finally {

            logger.info("end   callAnagraficheSigleStoreFunction ..");
        }

        return output;
    }
}
