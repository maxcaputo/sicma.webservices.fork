/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.coop.alleanza.sicma.webservices.ejbs;

import it.coop.alleanza.sicma.webservices.ejbs.anagrafichenegozi.AnagraficheNegoziBeanRemote;
import it.coop.alleanza.sicma.webservices.ejbs.anagrafichesigle.AnagraficheSigleBeanRemote;
import it.coop.alleanza.sicma.webservices.ejbs.checkupdates.CheckUpdatesBeanRemote;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import it.coop.alleanza.sicma.webservices.ejbs.livellomerceologico.LivelloMerceologicoBeanRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author antonio.caccamo
 */
public class SicmaEJBFactory extends EjbAbstractFactory {
    
    private static final Logger logger = LoggerFactory.getLogger(SicmaEJBFactory.class);

    private static final String ANAGRAFICHE_NEGOZI   =
            "ejb.AnagraficheNegoziBean#it.coop.alleanza.sicma.webservices.ejbs.anagrafichenegozi.AnagraficheNegoziBeanRemote";

    private static final String ANAGRAFICHE_SIGLE    =
            "ejb.AnagraficheSigleBean#it.coop.alleanza.sicma.webservices.ejbs.anagrafichesigle.AnagraficheSigleBeanRemote";

    private static final String LIVELLO_MERCEOLOGICO =
            "ejb.LivelloMerceologicoBean#it.coop.alleanza.sicma.webservices.ejbs.livellomerceologico.LivelloMerceologicoBeanRemote";

    private static final String CHECK_UPDATES =
            "ejb.CheckUpdatesBean#it.coop.alleanza.sicma.webservices.ejbs.checkupdates.CheckUpdatesBeanRemote";

    private static SicmaEJBFactory instance = null;

    private final InitialContext context;

    private SicmaEJBFactory() throws SicmaEJBException {
        try {
            context = new InitialContext();
        } catch (NamingException ex) {
            logger.error("error occurred during initializing context for ejb : ", ex);
            throw new SicmaEJBException(ex);
        }
    }

    public static synchronized SicmaEJBFactory getInstance() throws SicmaEJBException {
        if (instance == null) {
            instance = new SicmaEJBFactory();
        }
        return instance;
    }

    @Override
    public AnagraficheNegoziBeanRemote getAnagraficheNegoziBean() throws SicmaEJBException {
        return (AnagraficheNegoziBeanRemote) lookup(SicmaEJBFactory.ANAGRAFICHE_NEGOZI);
    }

    @Override
    public AnagraficheSigleBeanRemote getAnagraficheSigleBean() throws SicmaEJBException {
        return (AnagraficheSigleBeanRemote) lookup(SicmaEJBFactory.ANAGRAFICHE_SIGLE);
    }

    @Override
    public LivelloMerceologicoBeanRemote getLivelloMerceologicoBean() throws SicmaEJBException {
        return (LivelloMerceologicoBeanRemote) lookup(SicmaEJBFactory.LIVELLO_MERCEOLOGICO);
    }

    @Override
    public CheckUpdatesBeanRemote getCheckUpdatesBeanRemote() throws SicmaEJBException {
        return (CheckUpdatesBeanRemote) lookup(SicmaEJBFactory.CHECK_UPDATES);
    }


    private Object lookup(String name) throws SicmaEJBException {
        try {
            return context.lookup(name);
        } catch (NamingException ex) {
            logger.error("error occurred during context for ejb " +  name + " : ", ex);
            throw new SicmaEJBException(ex);
        }
    }

}
