package it.coop.allenza.sicma.webservices.anagrafiche.cercaarticoli.schema.input;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class FiltriRicerca implements Serializable {

    /**
     *
     * (Required)
     *
     */
    private FiltriRicerca.TrovaCorrelati trovaCorrelati;
    /**
     *
     * (Required)
     *
     */
    private FiltriRicerca.MostraFittizzi mostraFittizzi;
    /**
     *
     * (Required)
     *
     */
    private String descrizione;
    /**
     *
     * (Required)
     *
     */
    private FiltriRicerca.Stato stato;
    /**
     *
     * (Required)
     *
     */
    private Integer categoria;
    /**
     *
     * (Required)
     *
     */
    private Integer sottoCategoria;
    /**
     *
     * (Required)
     *
     */
    private Integer segmento;
    /**
     *
     * (Required)
     *
     */
    private String siglaProdotto;
    /**
     *
     * (Required)
     *
     */
    private String sottoSiglaProdotto;
    private final static long serialVersionUID = -5232808076041210392L;

    /**
     * No args constructor for use in serialization
     *
     */
    public FiltriRicerca() {
    }

    /**
     *
     * @param trovaCorrelati
     * @param sottoCategoria
     * @param categoria
     * @param stato
     * @param descrizione
     * @param mostraFittizzi
     * @param siglaProdotto
     * @param segmento
     * @param sottoSiglaProdotto
     */
    public FiltriRicerca(FiltriRicerca.TrovaCorrelati trovaCorrelati, FiltriRicerca.MostraFittizzi mostraFittizzi, String descrizione, FiltriRicerca.Stato stato, Integer categoria, Integer sottoCategoria, Integer segmento, String siglaProdotto, String sottoSiglaProdotto) {
        super();
        this.trovaCorrelati = trovaCorrelati;
        this.mostraFittizzi = mostraFittizzi;
        this.descrizione = descrizione;
        this.stato = stato;
        this.categoria = categoria;
        this.sottoCategoria = sottoCategoria;
        this.segmento = segmento;
        this.siglaProdotto = siglaProdotto;
        this.sottoSiglaProdotto = sottoSiglaProdotto;
    }

    /**
     *
     * (Required)
     *
     */
    public FiltriRicerca.TrovaCorrelati getTrovaCorrelati() {
        return trovaCorrelati;
    }

    /**
     *
     * (Required)
     *
     */
    public void setTrovaCorrelati(FiltriRicerca.TrovaCorrelati trovaCorrelati) {
        this.trovaCorrelati = trovaCorrelati;
    }

    public FiltriRicerca withTrovaCorrelati(FiltriRicerca.TrovaCorrelati trovaCorrelati) {
        this.trovaCorrelati = trovaCorrelati;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public FiltriRicerca.MostraFittizzi getMostraFittizzi() {
        return mostraFittizzi;
    }

    /**
     *
     * (Required)
     *
     */
    public void setMostraFittizzi(FiltriRicerca.MostraFittizzi mostraFittizzi) {
        this.mostraFittizzi = mostraFittizzi;
    }

    public FiltriRicerca withMostraFittizzi(FiltriRicerca.MostraFittizzi mostraFittizzi) {
        this.mostraFittizzi = mostraFittizzi;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     *
     * (Required)
     *
     */
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public FiltriRicerca withDescrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public FiltriRicerca.Stato getStato() {
        return stato;
    }

    /**
     *
     * (Required)
     *
     */
    public void setStato(FiltriRicerca.Stato stato) {
        this.stato = stato;
    }

    public FiltriRicerca withStato(FiltriRicerca.Stato stato) {
        this.stato = stato;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public Integer getCategoria() {
        return categoria;
    }

    /**
     *
     * (Required)
     *
     */
    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    public FiltriRicerca withCategoria(Integer categoria) {
        this.categoria = categoria;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public Integer getSottoCategoria() {
        return sottoCategoria;
    }

    /**
     *
     * (Required)
     *
     */
    public void setSottoCategoria(Integer sottoCategoria) {
        this.sottoCategoria = sottoCategoria;
    }

    public FiltriRicerca withSottoCategoria(Integer sottoCategoria) {
        this.sottoCategoria = sottoCategoria;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public Integer getSegmento() {
        return segmento;
    }

    /**
     *
     * (Required)
     *
     */
    public void setSegmento(Integer segmento) {
        this.segmento = segmento;
    }

    public FiltriRicerca withSegmento(Integer segmento) {
        this.segmento = segmento;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public String getSiglaProdotto() {
        return siglaProdotto;
    }

    /**
     *
     * (Required)
     *
     */
    public void setSiglaProdotto(String siglaProdotto) {
        this.siglaProdotto = siglaProdotto;
    }

    public FiltriRicerca withSiglaProdotto(String siglaProdotto) {
        this.siglaProdotto = siglaProdotto;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public String getSottoSiglaProdotto() {
        return sottoSiglaProdotto;
    }

    /**
     *
     * (Required)
     *
     */
    public void setSottoSiglaProdotto(String sottoSiglaProdotto) {
        this.sottoSiglaProdotto = sottoSiglaProdotto;
    }

    public FiltriRicerca withSottoSiglaProdotto(String sottoSiglaProdotto) {
        this.sottoSiglaProdotto = sottoSiglaProdotto;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(trovaCorrelati).append(mostraFittizzi).append(descrizione).append(stato).append(categoria).append(sottoCategoria).append(segmento).append(siglaProdotto).append(sottoSiglaProdotto).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof FiltriRicerca) == false) {
            return false;
        }
        FiltriRicerca rhs = ((FiltriRicerca) other);
        return new EqualsBuilder().append(trovaCorrelati, rhs.trovaCorrelati).append(mostraFittizzi, rhs.mostraFittizzi).append(descrizione, rhs.descrizione).append(stato, rhs.stato).append(categoria, rhs.categoria).append(sottoCategoria, rhs.sottoCategoria).append(segmento, rhs.segmento).append(siglaProdotto, rhs.siglaProdotto).append(sottoSiglaProdotto, rhs.sottoSiglaProdotto).isEquals();
    }

    public enum MostraFittizzi {

        S("S"),
        N("N");
        private final String value;
        private final static Map<String, FiltriRicerca.MostraFittizzi> CONSTANTS = new HashMap<String, FiltriRicerca.MostraFittizzi>();

        static {
            for (FiltriRicerca.MostraFittizzi c : values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private MostraFittizzi(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public String value() {
            return this.value;
        }

        public static FiltriRicerca.MostraFittizzi fromValue(String value) {
            FiltriRicerca.MostraFittizzi constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

    public enum Stato {

        A("A"),
        I("I"),
        R("R"),
        S("S");
        private final String value;
        private final static Map<String, FiltriRicerca.Stato> CONSTANTS = new HashMap<String, FiltriRicerca.Stato>();

        static {
            for (FiltriRicerca.Stato c : values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Stato(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public String value() {
            return this.value;
        }

        public static FiltriRicerca.Stato fromValue(String value) {
            FiltriRicerca.Stato constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

    public enum TrovaCorrelati {

        S("S"),
        N("N");
        private final String value;
        private final static Map<String, FiltriRicerca.TrovaCorrelati> CONSTANTS = new HashMap<String, FiltriRicerca.TrovaCorrelati>();

        static {
            for (FiltriRicerca.TrovaCorrelati c : values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private TrovaCorrelati(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public String value() {
            return this.value;
        }

        public static FiltriRicerca.TrovaCorrelati fromValue(String value) {
            FiltriRicerca.TrovaCorrelati constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
