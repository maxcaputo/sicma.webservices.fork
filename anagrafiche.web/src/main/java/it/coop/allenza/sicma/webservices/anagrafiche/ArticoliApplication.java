package it.coop.allenza.sicma.webservices.anagrafiche;

import it.coop.allenza.sicma.webservices.anagrafiche.anagrafichenegozi.AnagraficheNegoziRestService;
import it.coop.allenza.sicma.webservices.anagrafiche.anagrafichesigle.AnagraficheSigleRestService;
import it.coop.allenza.sicma.webservices.anagrafiche.cercaarticoli.CercaArticoliRestService;
import it.coop.allenza.sicma.webservices.anagrafiche.checkupdates.CheckUpdatesRestService;
import it.coop.allenza.sicma.webservices.anagrafiche.livellomerceologico.LivelloMerceologicoRestService;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/v1")
public class ArticoliApplication extends Application {
    
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("YYYYMMDD");

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        classes.add(AnagraficheNegoziRestService.class);
        classes.add(AnagraficheSigleRestService.class);
        classes.add(CercaArticoliRestService.class);
        classes.add(CheckUpdatesRestService.class);
        classes.add(LivelloMerceologicoRestService.class);
        return classes;
    }

}
