package it.coop.allenza.sicma.webservices.anagrafiche.cercaarticoli.schema.input;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class ArticoliPuntuali implements Serializable {

    /**
     *
     * (Required)
     *
     */
    private Integer codiceArticolo;
    private final static long serialVersionUID = -7226781673644438113L;

    /**
     * No args constructor for use in serialization
     *
     */
    public ArticoliPuntuali() {
    }

    /**
     *
     * @param codiceArticolo
     */
    public ArticoliPuntuali(Integer codiceArticolo) {
        super();
        this.codiceArticolo = codiceArticolo;
    }

    /**
     *
     * (Required)
     *
     */
    public Integer getCodiceArticolo() {
        return codiceArticolo;
    }

    /**
     *
     * (Required)
     *
     */
    public void setCodiceArticolo(Integer codiceArticolo) {
        this.codiceArticolo = codiceArticolo;
    }

    public ArticoliPuntuali withCodiceArticolo(Integer codiceArticolo) {
        this.codiceArticolo = codiceArticolo;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(codiceArticolo).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ArticoliPuntuali) == false) {
            return false;
        }
        ArticoliPuntuali rhs = ((ArticoliPuntuali) other);
        return new EqualsBuilder().append(codiceArticolo, rhs.codiceArticolo).isEquals();
    }

}
