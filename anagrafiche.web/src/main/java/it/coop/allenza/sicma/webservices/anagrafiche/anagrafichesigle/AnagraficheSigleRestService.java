package it.coop.allenza.sicma.webservices.anagrafiche.anagrafichesigle;

import it.coop.alleanza.sicma.webservices.ejbs.SicmaEJBFactory;
import it.coop.alleanza.sicma.webservices.ejbs.anagrafichesigle.AnagraficheSigleBeanRemote;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheOUTPUT;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/AnagraficheSigle")
public class AnagraficheSigleRestService {

    private static final Logger logger = LoggerFactory.getLogger(AnagraficheSigleRestService.class);

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response putAnagraficheSigle(AnagraficheINPUT input) {
        logger.info("input : " + input);
        Response response = null;
        AnagraficheOUTPUT output = null;
        try {
            AnagraficheSigleBeanRemote remote = SicmaEJBFactory.getInstance().getAnagraficheSigleBean();
            output = remote.callAnagraficheSigleStoreFunction(input);
            response = Response.status(Response.Status.OK).entity(output).build();
        } catch (SicmaEJBException ex) {
            logger.info("error occurred : ejb anagrafiche sigle :" + ex);
            output = new AnagraficheOUTPUT()
                    .withReturnStatus(Integer.valueOf(2))
                    .withReturnCode("999")
                    .withMessaggio(ex.getMessage())
            ;
            output.setReturnCode("999");
            response = Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(output).build();
        } catch (Exception ex) {
            logger.info("error occurred : " + ex);
            output = new AnagraficheOUTPUT()
                    .withReturnStatus(Integer.valueOf(2))
                    .withReturnCode("999")
                    .withMessaggio(ex.getMessage())
            ;
            response = Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(output).build();
        }
        logger.info("output : " + output);
        return response;
    }

}
