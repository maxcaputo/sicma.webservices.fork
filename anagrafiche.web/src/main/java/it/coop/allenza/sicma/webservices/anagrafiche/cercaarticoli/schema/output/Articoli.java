
package it.coop.allenza.sicma.webservices.anagrafiche.cercaarticoli.schema.output;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Articoli implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    private String codiceNegozio;
    /**
     * 
     * (Required)
     * 
     */
    private Integer codiceArticolo;
    /**
     * 
     * (Required)
     * 
     */
    private String padrePrezzi;
    /**
     * 
     * (Required)
     * 
     */
    private String descrizione;
    /**
     * 
     * (Required)
     * 
     */
    private Double prezzo;
    /**
     * 
     * (Required)
     * 
     */
    private String codiceLinearezzo;
    /**
     * 
     * (Required)
     * 
     */
    private String descrizioneLinea;
    /**
     * 
     * (Required)
     * 
     */
    private Articoli.StatoArticolo statoArticolo;
    /**
     * 
     * (Required)
     * 
     */
    private String flgArtPrezzato;
    /**
     * 
     * (Required)
     * 
     */
    private String refPesoVar;
    /**
     * 
     * (Required)
     * 
     */
    private Articoli.StatoAssortimentale statoAssortimentale;
    /**
     * 
     * (Required)
     * 
     */
    private Articoli.Fittizio fittizio;
    /**
     * 
     * (Required)
     * 
     */
    private Integer categoria;
    /**
     * 
     * (Required)
     * 
     */
    private Integer sottoCategoria;
    /**
     * 
     * (Required)
     * 
     */
    private Integer segmento;
    /**
     * 
     * (Required)
     * 
     */
    private String sigla;
    /**
     * 
     * (Required)
     * 
     */
    private String sottoSigla;
    private final static long serialVersionUID = -4031952298609253259L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Articoli() {
    }

    /**
     * 
     * @param refPesoVar
     * @param prezzo
     * @param flgArtPrezzato
     * @param codiceNegozio
     * @param fittizio
     * @param sottoCategoria
     * @param categoria
     * @param padrePrezzi
     * @param descrizione
     * @param codiceLinearezzo
     * @param statoAssortimentale
     * @param sigla
     * @param sottoSigla
     * @param statoArticolo
     * @param segmento
     * @param codiceArticolo
     * @param descrizioneLinea
     */
    public Articoli(String codiceNegozio, Integer codiceArticolo, String padrePrezzi, String descrizione, Double prezzo, String codiceLinearezzo, String descrizioneLinea, Articoli.StatoArticolo statoArticolo, String flgArtPrezzato, String refPesoVar, Articoli.StatoAssortimentale statoAssortimentale, Articoli.Fittizio fittizio, Integer categoria, Integer sottoCategoria, Integer segmento, String sigla, String sottoSigla) {
        super();
        this.codiceNegozio = codiceNegozio;
        this.codiceArticolo = codiceArticolo;
        this.padrePrezzi = padrePrezzi;
        this.descrizione = descrizione;
        this.prezzo = prezzo;
        this.codiceLinearezzo = codiceLinearezzo;
        this.descrizioneLinea = descrizioneLinea;
        this.statoArticolo = statoArticolo;
        this.flgArtPrezzato = flgArtPrezzato;
        this.refPesoVar = refPesoVar;
        this.statoAssortimentale = statoAssortimentale;
        this.fittizio = fittizio;
        this.categoria = categoria;
        this.sottoCategoria = sottoCategoria;
        this.segmento = segmento;
        this.sigla = sigla;
        this.sottoSigla = sottoSigla;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getCodiceNegozio() {
        return codiceNegozio;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setCodiceNegozio(String codiceNegozio) {
        this.codiceNegozio = codiceNegozio;
    }

    public Articoli withCodiceNegozio(String codiceNegozio) {
        this.codiceNegozio = codiceNegozio;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Integer getCodiceArticolo() {
        return codiceArticolo;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setCodiceArticolo(Integer codiceArticolo) {
        this.codiceArticolo = codiceArticolo;
    }

    public Articoli withCodiceArticolo(Integer codiceArticolo) {
        this.codiceArticolo = codiceArticolo;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getPadrePrezzi() {
        return padrePrezzi;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setPadrePrezzi(String padrePrezzi) {
        this.padrePrezzi = padrePrezzi;
    }

    public Articoli withPadrePrezzi(String padrePrezzi) {
        this.padrePrezzi = padrePrezzi;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Articoli withDescrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Double getPrezzo() {
        return prezzo;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setPrezzo(Double prezzo) {
        this.prezzo = prezzo;
    }

    public Articoli withPrezzo(Double prezzo) {
        this.prezzo = prezzo;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getCodiceLinearezzo() {
        return codiceLinearezzo;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setCodiceLinearezzo(String codiceLinearezzo) {
        this.codiceLinearezzo = codiceLinearezzo;
    }

    public Articoli withCodiceLinearezzo(String codiceLinearezzo) {
        this.codiceLinearezzo = codiceLinearezzo;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getDescrizioneLinea() {
        return descrizioneLinea;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setDescrizioneLinea(String descrizioneLinea) {
        this.descrizioneLinea = descrizioneLinea;
    }

    public Articoli withDescrizioneLinea(String descrizioneLinea) {
        this.descrizioneLinea = descrizioneLinea;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Articoli.StatoArticolo getStatoArticolo() {
        return statoArticolo;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setStatoArticolo(Articoli.StatoArticolo statoArticolo) {
        this.statoArticolo = statoArticolo;
    }

    public Articoli withStatoArticolo(Articoli.StatoArticolo statoArticolo) {
        this.statoArticolo = statoArticolo;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getFlgArtPrezzato() {
        return flgArtPrezzato;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setFlgArtPrezzato(String flgArtPrezzato) {
        this.flgArtPrezzato = flgArtPrezzato;
    }

    public Articoli withFlgArtPrezzato(String flgArtPrezzato) {
        this.flgArtPrezzato = flgArtPrezzato;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getRefPesoVar() {
        return refPesoVar;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setRefPesoVar(String refPesoVar) {
        this.refPesoVar = refPesoVar;
    }

    public Articoli withRefPesoVar(String refPesoVar) {
        this.refPesoVar = refPesoVar;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Articoli.StatoAssortimentale getStatoAssortimentale() {
        return statoAssortimentale;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setStatoAssortimentale(Articoli.StatoAssortimentale statoAssortimentale) {
        this.statoAssortimentale = statoAssortimentale;
    }

    public Articoli withStatoAssortimentale(Articoli.StatoAssortimentale statoAssortimentale) {
        this.statoAssortimentale = statoAssortimentale;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Articoli.Fittizio getFittizio() {
        return fittizio;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setFittizio(Articoli.Fittizio fittizio) {
        this.fittizio = fittizio;
    }

    public Articoli withFittizio(Articoli.Fittizio fittizio) {
        this.fittizio = fittizio;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Integer getCategoria() {
        return categoria;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    public Articoli withCategoria(Integer categoria) {
        this.categoria = categoria;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Integer getSottoCategoria() {
        return sottoCategoria;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setSottoCategoria(Integer sottoCategoria) {
        this.sottoCategoria = sottoCategoria;
    }

    public Articoli withSottoCategoria(Integer sottoCategoria) {
        this.sottoCategoria = sottoCategoria;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Integer getSegmento() {
        return segmento;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setSegmento(Integer segmento) {
        this.segmento = segmento;
    }

    public Articoli withSegmento(Integer segmento) {
        this.segmento = segmento;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public Articoli withSigla(String sigla) {
        this.sigla = sigla;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public String getSottoSigla() {
        return sottoSigla;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setSottoSigla(String sottoSigla) {
        this.sottoSigla = sottoSigla;
    }

    public Articoli withSottoSigla(String sottoSigla) {
        this.sottoSigla = sottoSigla;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(codiceNegozio).append(codiceArticolo).append(padrePrezzi).append(descrizione).append(prezzo).append(codiceLinearezzo).append(descrizioneLinea).append(statoArticolo).append(flgArtPrezzato).append(refPesoVar).append(statoAssortimentale).append(fittizio).append(categoria).append(sottoCategoria).append(segmento).append(sigla).append(sottoSigla).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Articoli) == false) {
            return false;
        }
        Articoli rhs = ((Articoli) other);
        return new EqualsBuilder().append(codiceNegozio, rhs.codiceNegozio).append(codiceArticolo, rhs.codiceArticolo).append(padrePrezzi, rhs.padrePrezzi).append(descrizione, rhs.descrizione).append(prezzo, rhs.prezzo).append(codiceLinearezzo, rhs.codiceLinearezzo).append(descrizioneLinea, rhs.descrizioneLinea).append(statoArticolo, rhs.statoArticolo).append(flgArtPrezzato, rhs.flgArtPrezzato).append(refPesoVar, rhs.refPesoVar).append(statoAssortimentale, rhs.statoAssortimentale).append(fittizio, rhs.fittizio).append(categoria, rhs.categoria).append(sottoCategoria, rhs.sottoCategoria).append(segmento, rhs.segmento).append(sigla, rhs.sigla).append(sottoSigla, rhs.sottoSigla).isEquals();
    }

    public enum Fittizio {

        S("S"),
        N("N");
        private final String value;
        private final static Map<String, Articoli.Fittizio> CONSTANTS = new HashMap<String, Articoli.Fittizio>();

        static {
            for (Articoli.Fittizio c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Fittizio(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public String value() {
            return this.value;
        }

        public static Articoli.Fittizio fromValue(String value) {
            Articoli.Fittizio constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

    public enum StatoArticolo {

        A("A"),
        I("I"),
        R("R"),
        S("S");
        private final String value;
        private final static Map<String, Articoli.StatoArticolo> CONSTANTS = new HashMap<String, Articoli.StatoArticolo>();

        static {
            for (Articoli.StatoArticolo c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private StatoArticolo(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public String value() {
            return this.value;
        }

        public static Articoli.StatoArticolo fromValue(String value) {
            Articoli.StatoArticolo constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

    public enum StatoAssortimentale {

        A("A"),
        S("S");
        private final String value;
        private final static Map<String, Articoli.StatoAssortimentale> CONSTANTS = new HashMap<String, Articoli.StatoAssortimentale>();

        static {
            for (Articoli.StatoAssortimentale c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private StatoAssortimentale(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public String value() {
            return this.value;
        }

        public static Articoli.StatoAssortimentale fromValue(String value) {
            Articoli.StatoAssortimentale constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
