/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.coop.allenza.sicma.webservices.anagrafiche.livellomerceologico;

import it.coop.alleanza.sicma.webservices.ejbs.SicmaEJBFactory;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheOUTPUT;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import it.coop.alleanza.sicma.webservices.ejbs.livellomerceologico.LivelloMerceologicoBeanRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author antonio.caccamo
 */
@Path("/LivelloMerceologico")
public class LivelloMerceologicoRestService {

    private static final Logger logger = LoggerFactory.getLogger(LivelloMerceologicoRestService.class);

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response putLivelloMerceologico(AnagraficheINPUT input) {
        logger.info("input : " + input);
        Response response = null;
        AnagraficheOUTPUT output = null;
        try {
            LivelloMerceologicoBeanRemote remote = SicmaEJBFactory.getInstance().getLivelloMerceologicoBean();
            output = remote.callLivelloMerceologicoStoreFunction(input);
            response = Response.status(Response.Status.OK).entity(output).build();
        } catch (SicmaEJBException ex) {
            logger.info("error occurred : ejb anagrafiche sigle :" + ex);
            output = new AnagraficheOUTPUT()
                    .withReturnStatus(Integer.valueOf(2))
                    .withReturnCode("999")
                    .withMessaggio(ex.getMessage())
            ;
            output.setReturnCode("999");
            response = Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(output).build();
        } catch (Exception ex) {
            logger.info("error occurred : " + ex);
            output = new AnagraficheOUTPUT()
                    .withReturnStatus(Integer.valueOf(2))
                    .withReturnCode("999")
                    .withMessaggio(ex.getMessage())
            ;
            response = Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(output).build();
        }
        logger.info("output : " + output);
        return response;
    }

}
