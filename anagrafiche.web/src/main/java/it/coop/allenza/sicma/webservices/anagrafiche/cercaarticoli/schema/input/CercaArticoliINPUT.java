package it.coop.allenza.sicma.webservices.anagrafiche.cercaarticoli.schema.input;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 
 * @author antonio.caccamo
 */
@XmlRootElement
public class CercaArticoliINPUT implements Serializable {

    /**
     *
     * (Required)
     *
     */
    private String codiceAzienda;
    /**
     *
     * (Required)
     *
     */
    private String codiceCanale;
    /**
     *
     * (Required)
     *
     */
    private Integer requestId;
    /**
     *
     * (Required)
     *
     */
    private String codiceNegozio;
    /**
     *
     * (Required)
     *
     */
    private String dataRiferimento;
    /**
     *
     * (Required)
     *
     */
    private Integer maxResults;
    /**
     *
     * (Required)
     *
     */
    private String client;
    /**
     *
     * (Required)
     *
     */
    private String versione;
    /**
     *
     * (Required)
     *
     */
    private Integer userCodeID;
    /**
     *
     * (Required)
     *
     */
    private List<ArticoliPuntuali> articoliPuntuali = null;
    /**
     *
     * (Required)
     *
     */
    private FiltriRicerca filtriRicerca;
    private final static long serialVersionUID = 3506672096528973918L;

    /**
     * No args constructor for use in serialization
     *
     */
    public CercaArticoliINPUT() {
    }

    /**
     *
     * @param codiceCanale
     * @param filtriRicerca
     * @param client
     * @param requestId
     * @param versione
     * @param articoliPuntuali
     * @param userCodeID
     * @param codiceAzienda
     * @param dataRiferimento
     * @param maxResults
     * @param codiceNegozio
     */
    public CercaArticoliINPUT(String codiceAzienda, String codiceCanale, Integer requestId, String codiceNegozio, String dataRiferimento, Integer maxResults, String client, String versione, Integer userCodeID, List<ArticoliPuntuali> articoliPuntuali, FiltriRicerca filtriRicerca) {
        super();
        this.codiceAzienda = codiceAzienda;
        this.codiceCanale = codiceCanale;
        this.requestId = requestId;
        this.codiceNegozio = codiceNegozio;
        this.dataRiferimento = dataRiferimento;
        this.maxResults = maxResults;
        this.client = client;
        this.versione = versione;
        this.userCodeID = userCodeID;
        this.articoliPuntuali = articoliPuntuali;
        this.filtriRicerca = filtriRicerca;
    }

    /**
     *
     * (Required)
     *
     */
    public String getCodiceAzienda() {
        return codiceAzienda;
    }

    /**
     *
     * (Required)
     *
     */
    public void setCodiceAzienda(String codiceAzienda) {
        this.codiceAzienda = codiceAzienda;
    }

    public CercaArticoliINPUT withCodiceAzienda(String codiceAzienda) {
        this.codiceAzienda = codiceAzienda;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public String getCodiceCanale() {
        return codiceCanale;
    }

    /**
     *
     * (Required)
     *
     */
    public void setCodiceCanale(String codiceCanale) {
        this.codiceCanale = codiceCanale;
    }

    public CercaArticoliINPUT withCodiceCanale(String codiceCanale) {
        this.codiceCanale = codiceCanale;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public Integer getRequestId() {
        return requestId;
    }

    /**
     *
     * (Required)
     *
     */
    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public CercaArticoliINPUT withRequestId(Integer requestId) {
        this.requestId = requestId;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public String getCodiceNegozio() {
        return codiceNegozio;
    }

    /**
     *
     * (Required)
     *
     */
    public void setCodiceNegozio(String codiceNegozio) {
        this.codiceNegozio = codiceNegozio;
    }

    public CercaArticoliINPUT withCodiceNegozio(String codiceNegozio) {
        this.codiceNegozio = codiceNegozio;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public String getDataRiferimento() {
        return dataRiferimento;
    }

    /**
     *
     * (Required)
     *
     */
    public void setDataRiferimento(String dataRiferimento) {
        this.dataRiferimento = dataRiferimento;
    }

    public CercaArticoliINPUT withDataRiferimento(String dataRiferimento) {
        this.dataRiferimento = dataRiferimento;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public Integer getMaxResults() {
        return maxResults;
    }

    /**
     *
     * (Required)
     *
     */
    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    public CercaArticoliINPUT withMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public String getClient() {
        return client;
    }

    /**
     *
     * (Required)
     *
     */
    public void setClient(String client) {
        this.client = client;
    }

    public CercaArticoliINPUT withClient(String client) {
        this.client = client;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public String getVersione() {
        return versione;
    }

    /**
     *
     * (Required)
     *
     */
    public void setVersione(String versione) {
        this.versione = versione;
    }

    public CercaArticoliINPUT withVersione(String versione) {
        this.versione = versione;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public Integer getUserCodeID() {
        return userCodeID;
    }

    /**
     *
     * (Required)
     *
     */
    public void setUserCodeID(Integer userCodeID) {
        this.userCodeID = userCodeID;
    }

    public CercaArticoliINPUT withUserCodeID(Integer userCodeID) {
        this.userCodeID = userCodeID;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public List<ArticoliPuntuali> getArticoliPuntuali() {
        return articoliPuntuali;
    }

    /**
     *
     * (Required)
     *
     */
    public void setArticoliPuntuali(List<ArticoliPuntuali> articoliPuntuali) {
        this.articoliPuntuali = articoliPuntuali;
    }

    public CercaArticoliINPUT withArticoliPuntuali(List<ArticoliPuntuali> articoliPuntuali) {
        this.articoliPuntuali = articoliPuntuali;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    public FiltriRicerca getFiltriRicerca() {
        return filtriRicerca;
    }

    /**
     *
     * (Required)
     *
     */
    public void setFiltriRicerca(FiltriRicerca filtriRicerca) {
        this.filtriRicerca = filtriRicerca;
    }

    public CercaArticoliINPUT withFiltriRicerca(FiltriRicerca filtriRicerca) {
        this.filtriRicerca = filtriRicerca;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(codiceAzienda).append(codiceCanale).append(requestId).append(codiceNegozio).append(dataRiferimento).append(maxResults).append(client).append(versione).append(userCodeID).append(articoliPuntuali).append(filtriRicerca).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CercaArticoliINPUT) == false) {
            return false;
        }
        CercaArticoliINPUT rhs = ((CercaArticoliINPUT) other);
        return new EqualsBuilder().append(codiceAzienda, rhs.codiceAzienda).append(codiceCanale, rhs.codiceCanale).append(requestId, rhs.requestId).append(codiceNegozio, rhs.codiceNegozio).append(dataRiferimento, rhs.dataRiferimento).append(maxResults, rhs.maxResults).append(client, rhs.client).append(versione, rhs.versione).append(userCodeID, rhs.userCodeID).append(articoliPuntuali, rhs.articoliPuntuali).append(filtriRicerca, rhs.filtriRicerca).isEquals();
    }

}
