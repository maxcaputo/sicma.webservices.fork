package it.coop.allenza.sicma.webservices.anagrafiche.cercaarticoli;

import it.coop.allenza.sicma.webservices.anagrafiche.cercaarticoli.schema.input.CercaArticoliINPUT;
import it.coop.allenza.sicma.webservices.anagrafiche.cercaarticoli.schema.output.CercaArticoliOUTPUT;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/CercaArticoli")
public class CercaArticoliRestService {

    private static final Logger logger = LoggerFactory.getLogger(CercaArticoliRestService.class);

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response putCercaArticoli(CercaArticoliINPUT input) throws NamingException {

        logger.info("input : " + input);        

        final CercaArticoliOUTPUT output = new CercaArticoliOUTPUT()
                .withRequestId(input.getRequestId())
                .withReturnStatus(Integer.valueOf(0))
                .withMessaggio(StringUtils.EMPTY)     ;

        logger.info("output : " + output);

        return Response.ok(output).build();
    }
    
}
