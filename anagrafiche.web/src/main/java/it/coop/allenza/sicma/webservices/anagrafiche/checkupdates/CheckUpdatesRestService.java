package it.coop.allenza.sicma.webservices.anagrafiche.checkupdates;

import it.coop.alleanza.sicma.webservices.ejbs.SicmaEJBFactory;
import it.coop.alleanza.sicma.webservices.ejbs.checkupdates.CheckUpdatesBeanRemote;
import it.coop.alleanza.sicma.webservices.ejbs.dto.AnagraficheINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.dto.CheckUpdatesOUTPUT;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/CheckUpdates")
public class CheckUpdatesRestService {
    
    private static final Logger logger = LoggerFactory.getLogger(CheckUpdatesRestService.class);
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response putCheckUpdates(AnagraficheINPUT input) throws Exception {
        logger.info("input : " + input);
        Response response = null;
        CheckUpdatesOUTPUT output = null;
        try {
            CheckUpdatesBeanRemote remote = SicmaEJBFactory.getInstance().getCheckUpdatesBeanRemote();
            output = remote.callCheckUpdatesStoreFunction(input);
            response = Response.status(Response.Status.OK).entity(output).build();
        } catch (SicmaEJBException ex) {
            logger.info("error occurred : ejb anagrafiche sigle :" + ex);
            output = new CheckUpdatesOUTPUT()
                    .withReturnStatus(Integer.valueOf(2))
                    .withReturnCode("999")
                    .withMessaggio(ex.getMessage())
            ;
            output.setReturnCode("999");
            response = Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(output).build();
        } catch (Exception ex) {
            logger.info("error occurred : " + ex);
            output = new CheckUpdatesOUTPUT()
                    .withReturnStatus(Integer.valueOf(2))
                    .withReturnCode("999")
                    .withMessaggio(ex.getMessage())
            ;
            response = Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(output).build();
        }
        logger.info("output : " + output);
        return response;
    }

  

}
