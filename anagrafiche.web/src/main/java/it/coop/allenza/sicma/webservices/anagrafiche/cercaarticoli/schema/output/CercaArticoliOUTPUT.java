
package it.coop.allenza.sicma.webservices.anagrafiche.cercaarticoli.schema.output;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@XmlRootElement
public class CercaArticoliOUTPUT implements Serializable
{

    private String codiceAzienda;
    private String codiceCanale;
    /**
     * 
     * (Required)
     * 
     */
    private Integer requestId;
    /**
     * 
     * (Required)
     * 
     */
    private Integer returnStatus;
    private String returnCode;
    private String messaggio;
    private List<Articoli> articoli = null;
    private final static long serialVersionUID = 6510397840542828540L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CercaArticoliOUTPUT() {
    }

    /**
     * 
     * @param returnCode
     * @param codiceCanale
     * @param requestId
     * @param codiceAzienda
     * @param returnStatus
     * @param messaggio
     * @param articoli
     */
    public CercaArticoliOUTPUT(String codiceAzienda, String codiceCanale, Integer requestId, Integer returnStatus, String returnCode, String messaggio, List<Articoli> articoli) {
        super();
        this.codiceAzienda = codiceAzienda;
        this.codiceCanale = codiceCanale;
        this.requestId = requestId;
        this.returnStatus = returnStatus;
        this.returnCode = returnCode;
        this.messaggio = messaggio;
        this.articoli = articoli;
    }

    public String getCodiceAzienda() {
        return codiceAzienda;
    }

    public void setCodiceAzienda(String codiceAzienda) {
        this.codiceAzienda = codiceAzienda;
    }

    public CercaArticoliOUTPUT withCodiceAzienda(String codiceAzienda) {
        this.codiceAzienda = codiceAzienda;
        return this;
    }

    public String getCodiceCanale() {
        return codiceCanale;
    }

    public void setCodiceCanale(String codiceCanale) {
        this.codiceCanale = codiceCanale;
    }

    public CercaArticoliOUTPUT withCodiceCanale(String codiceCanale) {
        this.codiceCanale = codiceCanale;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Integer getRequestId() {
        return requestId;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public CercaArticoliOUTPUT withRequestId(Integer requestId) {
        this.requestId = requestId;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    public Integer getReturnStatus() {
        return returnStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    public void setReturnStatus(Integer returnStatus) {
        this.returnStatus = returnStatus;
    }

    public CercaArticoliOUTPUT withReturnStatus(Integer returnStatus) {
        this.returnStatus = returnStatus;
        return this;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public CercaArticoliOUTPUT withReturnCode(String returnCode) {
        this.returnCode = returnCode;
        return this;
    }

    public String getMessaggio() {
        return messaggio;
    }

    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

    public CercaArticoliOUTPUT withMessaggio(String messaggio) {
        this.messaggio = messaggio;
        return this;
    }

    public List<Articoli> getArticoli() {
        return articoli;
    }

    public void setArticoli(List<Articoli> articoli) {
        this.articoli = articoli;
    }

    public CercaArticoliOUTPUT withArticoli(List<Articoli> articoli) {
        this.articoli = articoli;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(codiceAzienda).append(codiceCanale).append(requestId).append(returnStatus).append(returnCode).append(messaggio).append(articoli).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CercaArticoliOUTPUT) == false) {
            return false;
        }
        CercaArticoliOUTPUT rhs = ((CercaArticoliOUTPUT) other);
        return new EqualsBuilder().append(codiceAzienda, rhs.codiceAzienda).append(codiceCanale, rhs.codiceCanale).append(requestId, rhs.requestId).append(returnStatus, rhs.returnStatus).append(returnCode, rhs.returnCode).append(messaggio, rhs.messaggio).append(articoli, rhs.articoli).isEquals();
    }

}
