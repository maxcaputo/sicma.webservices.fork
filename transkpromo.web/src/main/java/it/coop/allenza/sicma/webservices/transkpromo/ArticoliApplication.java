package it.coop.allenza.sicma.webservices.transkpromo;

import it.coop.allenza.sicma.webservices.transkpromo.transkpromo.TransKPromoRestService;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/v1")
public class ArticoliApplication extends Application {
    
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("YYYYMMDD");

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        classes.add(TransKPromoRestService.class);
        return classes;
    }

}
