/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.coop.allenza.sicma.webservices.transkpromo.transkpromo;

import it.coop.alleanza.sicma.webservices.ejbs.SicmaEJBFactory;
import it.coop.alleanza.sicma.webservices.ejbs.exception.SicmaEJBException;
import it.coop.alleanza.sicma.webservices.ejbs.transkpromo.TransKPromoBeanRemote;
import it.coop.alleanza.sicma.webservices.ejbs.transkpromo.dto.TransKPromoINPUT;
import it.coop.alleanza.sicma.webservices.ejbs.transkpromo.dto.TransKPromoOUTPUT;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author antonio.caccamo
 */
@Path("/TransKPromo")
public class TransKPromoRestService {

    private static final Logger logger = LoggerFactory.getLogger(TransKPromoRestService.class);

    private static final String IPER = "I";
    private static final String SUPER = "S";

    /**
     *
     * @param input
     * @return
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response putTransKPromo(TransKPromoINPUT input) {
        logger.info("input  : " + input);
        TransKPromoOUTPUT output = null;
        Response response = null;

        try {
            final TransKPromoBeanRemote remote = SicmaEJBFactory.getInstance().getTransKPromoBean();
            if (isValidInput(input)) {
                if (input.getCodiceCanale().startsWith(IPER)) {
                    output = remote.callTransKPromoStoreProcedureIPER(input);
                }
                if (input.getCodiceCanale().startsWith(SUPER)) {
                    output = remote.callTransKPromoStoreProcedureSUPER(input);
                }
                response = Response.ok(output).build();
            } else {
                logger.error("no valid input received : " + input);
                output = new TransKPromoOUTPUT();
                output.setCodiceAzienda(input.getCodiceAzienda());
                output.setCodiceCanale(input.getCodiceCanale());
                output.setRequestId(input.getRequestId());                
                output.setReturnStatus(Integer.valueOf(1));
                output.setReturnCode("002");
                output.setMessaggio("Argomenti insufficienti");
                response = Response.status(Response.Status.BAD_REQUEST).entity(output).build();
            }
        } catch (SicmaEJBException ex) {
            logger.info("error occurred : ejb trankpromo :" + ex);
            output = new TransKPromoOUTPUT();
            output.setReturnStatus(Integer.valueOf(2));
            output.setReturnCode("999");
            output.setMessaggio(ex.getMessage());
            response = Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(output).build();
        } catch (Exception ex) {
            logger.info("error occurred : " + ex);
            output = new TransKPromoOUTPUT();
            output.setReturnStatus(Integer.valueOf(2));
            output.setReturnCode(ex.getMessage());
            output.setMessaggio(StringUtils.EMPTY);
            response = Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(output).build();
        }
        logger.info("output  : " + output);
        return response;
    }

    private boolean isValidInput(TransKPromoINPUT input) {
        
        if ( input.getCodiceAzienda() == null ) {
            logger.error("codiceAzienda is null");
            return false;
        }
        
        if ( input.getCodiceCanale() == null ) {
            logger.error("codiceCanale is null");
            return false;
        }
        
        if ( input.getTipoDato() == null ) {
            logger.error("tipoDato is null");
            return false;
        }
        
        if ( "ARTICOLI".equalsIgnoreCase( input.getTipoDato()) ) {
            if ( input.getCodiceNegozio() == null ) {
                logger.error("codiceNegozio is null");
                return false;
            }
        }
        
        if ( input.getClient() == null ) {
            logger.error("client is null");
            return false;
        }
        
        if ( input.getTransCodifiche() == null || input.getTransCodifiche().size() == 0 ) {
            logger.error("transCodiche is empty");
            return false;
        }
        
        return true;
    }

}
