package it.coop.alleanza.sicma.webservices.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "DELIVERY_HEADER_PAID")
@NamedNativeQuery(
    name = DeliveryHeaderPaid.WS_DECODIFICA_ANAGRAFICA_GET_DELIVERIES_HEADER_PAID,        
    query = "{ ? = call WS_DECODIFICA_ANAGRAFICA.GET_DELIVERIES_HEADER_PAID( ? ) }",     
    resultSetMapping = "deliveryHeaderPaidResultSetMapping"
        
)
@SqlResultSetMapping(
    name = "deliveryHeaderPaidResultSetMapping",
        entities = {
            @EntityResult(entityClass = DeliveryHeaderPaid.class)
        }
)
public class DeliveryHeaderPaid {
    private final static long serialVersionUID = -6177745227429015904L;
    public static final String WS_DECODIFICA_ANAGRAFICA_GET_DELIVERIES_HEADER_PAID = 
            "WS_DECODIFICA_ANAGRAFICA.GET_DELIVERIES_HEADER_PAID";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DELIVERY_HEADER_PAID_SEQ_GEN")
    @SequenceGenerator(name = "DELIVERY_HEADER_PAID_SEQ_GEN", sequenceName = "DELIVERY_HEADER_PAID_SEQ", allocationSize = 1)
    @Column(name = "delivery_entry_nr")
    private Long deliveryEntryNumber;
    /**
     *
     * (Required)
     *
     */
    @Column(name = "darkstore_code")    
    private String darkstoreCode;
    /**
     *
     * (Required)
     *
     */
    @Column(name = "order_number")    
    private String orderNumber;
    /**
     *
     * (Required)
     *
     */
    @Column(name = "cart_status")    
    private String cartStatus;

    @Column(name = "total_order_paid")    
    private Double totalOrderPaid;


    /**
     * No args constructor for use in serialization
     *
     */
    public DeliveryHeaderPaid() {
    }

    /**
     *
     * @param darkstoreCode
     * @param totalOrderPaid
     * @param cartStatus
     * @param orderNumber
     */
    public DeliveryHeaderPaid(String darkstoreCode, String orderNumber, String cartStatus, Double totalOrderPaid) {
        super();
        this.darkstoreCode = darkstoreCode;
        this.orderNumber = orderNumber;
        this.cartStatus = cartStatus;
        this.totalOrderPaid = totalOrderPaid;
    }

    @XmlTransient
    public Long getDeliveryEntryNumber() {
        return deliveryEntryNumber;
    }

    public void setDeliveryEntryNumber(Long deliveryEntryNumber) {
        this.deliveryEntryNumber = deliveryEntryNumber;
    }

    /**
     *
     * (Required)
     *
     */
    @XmlElement(name = "darkstore_code")
    public String getDarkstoreCode() {
        return darkstoreCode;
    }

    /**
     *
     * (Required)
     *
     */
    public void setDarkstoreCode(String darkstoreCode) {
        this.darkstoreCode = darkstoreCode;
    }

    public DeliveryHeaderPaid withDarkstoreCode(String darkstoreCode) {
        this.darkstoreCode = darkstoreCode;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @XmlElement(name = "order_number")
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     *
     * (Required)
     *
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public DeliveryHeaderPaid withOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @XmlElement(name = "cart_status")
    public String getCartStatus() {
        return cartStatus;
    }

    /**
     *
     * (Required)
     *
     */
    public void setCartStatus(String cartStatus) {
        this.cartStatus = cartStatus;
    }

    public DeliveryHeaderPaid withCartStatus(String cartStatus) {
        this.cartStatus = cartStatus;
        return this;
    }

    @XmlElement(name = "total_order_paid")
    public Double getTotalOrderPaid() {
        return totalOrderPaid;
    }

    public void setTotalOrderPaid(Double totalOrderPaid) {
        this.totalOrderPaid = totalOrderPaid;
    }

    public DeliveryHeaderPaid withTotalOrderPaid(Double totalOrderPaid) {
        this.totalOrderPaid = totalOrderPaid;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(deliveryEntryNumber).append(darkstoreCode).append(orderNumber).append(cartStatus).append(totalOrderPaid).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DeliveryHeaderPaid) == false) {
            return false;
        }
        DeliveryHeaderPaid rhs = ((DeliveryHeaderPaid) other);
        return new EqualsBuilder().append(darkstoreCode, rhs.darkstoreCode).append(orderNumber, rhs.orderNumber).append(cartStatus, rhs.cartStatus).append(totalOrderPaid, rhs.totalOrderPaid).isEquals();
    }

}
