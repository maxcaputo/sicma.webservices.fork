
package it.coop.alleanza.sicma.webservices.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author antonio.caccamo
 */
@Entity
@Table(name = "DECODIFICA_ANAGRAFICA")
@IdClass(DecodificaAnagraficaPK.class)
public class DecodificaAnagrafica implements Serializable{

    private static final long serialVersionUID = 0L;
    
   @Id @Column(name = "TIPO_DATO")
    private String tipoDato;

    @Id @Column(name = "COD_COOPERATIVA")
    private Integer codiceCooperativa;

    @Id @Column(name = "COD_NEGOZIO")
    private Integer codiceNegozio;

    @Id @Column(name = "COD_ANAGR_NEGOZIO")
    private Integer CodiceAngraficaNegozio;

    @Column(name = "COD_ANAGR_SICMA")
    private Integer codiceAngraficaSicma;

    public String getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    public Integer getCodiceCooperativa() {
        return codiceCooperativa;
    }

    public void setCodiceCooperativa(Integer codiceCooperativa) {
        this.codiceCooperativa = codiceCooperativa;
    }

    public Integer getCodiceNegozio() {
        return codiceNegozio;
    }

    public void setCodiceNegozio(Integer codiceNegozio) {
        this.codiceNegozio = codiceNegozio;
    }

    public Integer getCodiceAngraficaNegozio() {
        return CodiceAngraficaNegozio;
    }

    public void setCodiceAngraficaNegozio(Integer CodiceAngraficaNegozio) {
        this.CodiceAngraficaNegozio = CodiceAngraficaNegozio;
    }

    public Integer getCodiceAngraficaSicma() {
        return codiceAngraficaSicma;
    }

    public void setCodiceAngraficaSicma(Integer codiceAngraficaSicma) {
        this.codiceAngraficaSicma = codiceAngraficaSicma;
    }

    @Override
    public String toString() {
        return "DecodificaAnagrafica{" + "tipoDato=" + tipoDato + ", codiceCooperativa=" + codiceCooperativa + ", codiceNegozio=" + codiceNegozio + ", CodiceAngraficaNegozio=" + CodiceAngraficaNegozio + ", codiceAngraficaSicma=" + codiceAngraficaSicma + '}';
    }        
               
}
